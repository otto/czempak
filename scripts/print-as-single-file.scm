
(use-modules (ice-9 pretty-print))

(define args (command-line))
(define without-self (cdr args))
(define target-file (car without-self))
(define search-paths (cdr without-self))

;; (write target-file) (newline)
;; (write search-paths) (newline)

(define read-list
  (case-lambda
   (() (read-list (current-input-port)))
   ((p)
    (let lp ()
      (let ((r (read p)))
        (if (eof-object? r)
            '()
            (cons r (lp))))))))

(define (read-file-as-list path)
  (define p (open-file path "r"))
  (define ret (read-list p))
  (close-port p)
  ret)

(define H (make-hash-table))

(define (absolute-posix-path? path)
  (and (string? path)
       (> (string-length path) 0)
       (char=? (string-ref path 0) #\/)))

(define (list-remove-common-prefix as bs)
  (let loop ((as as) (bs bs))
    (cond
     ((null? as) as)
     ((null? bs) as)
     ((eq? (car as) (car bs))
      (loop (cdr as) (cdr bs)))
     (else as))))

(define (remove-common-prefix a b)
  (cond
   ((string? a)
    (list->string
     (list-remove-common-prefix (string->list a) (string->list b))))
   ((list? a)
    (list-remove-common-prefix a b))
   (else
    (throw 'expecting-string-or-list a b))))

(define [append-posix-path2 a b]
  (if (= (string-length a) 0)
      b
      (let ((b
             (if (absolute-posix-path? b)
                 (let ((cl (remove-common-prefix b a)))
                   (if (equal? cl b)
                       (throw 'append-posix-path-disjoint `(args: ,a ,b))
                       cl))
                 b)))
        (if (char=? #\/ (string-ref a (1- (string-length a))))
            (string-append a b)
            (string-append a "/" b)))))

(define [append-posix-path . paths]
  (if (null? paths) "/"
      (let loop ((paths paths))
        (if (null? (cdr paths)) (car paths)
            (append-posix-path2 (car paths) (loop (cdr paths)))))))


(define (handle-1-module/true module)
  (define strings (map symbol->string module))
  (define path0 (apply append-posix-path strings))
  (define path (string-append path0 ".scm"))
  (define abss
    (map
     (lambda (search-path)
       (define abs (append-posix-path search-path path))
       (and (file-exists? abs)
            (begin
              (parameterize ((current-output-port (current-error-port)))
                (display "Import: ") (write abs) (newline))
              (print-file abs)
              #t)))
     search-paths))
  (define matches (filter identity abss))

  (when (null? matches)
    (parameterize ((current-output-port (current-error-port)))
      (display "Not found: ") (write module) (newline)))

  (and (null? matches)
       module))

(define (handle-1-module module)
  (if (symbol? (car module))
      (handle-1-module/true module)
      (let ((got (handle-1-module/true (car module))))
        (and got
             (cons (handle-1-module/true (car module))
                   (cdr module))))))

(define (handle-modules modules)
  (define result
    (filter
     identity
     (map handle-1-module modules)))
  (unless (null? result)
    (write (cons 'use-modules result))))

(define (print-file filepath)
  (unless (hash-ref H filepath #f)
    (hash-set! H filepath #t)
    (let ((current (read-file-as-list filepath)))
      (for-each
       (lambda (x)
         (case (car x)
           ((use-modules) (handle-modules (cdr x)))
           ((define-module) 'SKIP-IT)
           (else (pretty-print x)))
         (newline) (newline))
       current))))

(print-file target-file)
