#! /bin/sh

CURDIR="$(dirname "$0")"
ROOTDIR="$(readlink -f "$CURDIR/..")"
SRCDIR="$(readlink -f "$ROOTDIR/src")"
DISTDIR="$(readlink -f "$ROOTDIR/dist")"
EUPHRATESDIR="$SRCDIR/euphrates/build/src/guile"

echo "#! /bin/sh
exec guile -L '$SRCDIR' -L '$EUPHRATESDIR' -s '$SRCDIR/main.scm' \"\$@\"" > "$DISTDIR/czempak"

chmod +x "$DISTDIR/czempak"
