
PREFIX = $(HOME)/.local
PREFIXBIN = $(PREFIX)/bin

BINARYPATH = $(PREFIXBIN)/czempak

install: $(BINARYPATH)

$(BINARYPATH) $(BINARYPATH0): $(PREFIXBIN) all
	cp dist/czempak $(BINARYPATH)

all: | dist src/euphrates/build dist/czempak

dist/czempak: src/*.scm src/czempaklib/*.scm
	scripts/make-czempak.sh

dist:
	mkdir dist/

src/hashing/README.md:
	git submodule update --init

src/euphrates/build:
	cd src/euphrates && $(MAKE)

$(PREFIXBIN):
	mkdir -p "$@"

reinstall: clean
	$(MAKE) install

clean:
	git submodule foreach --recursive 'git clean -dfx'
	git clean -dfx

.PHONY: all install reinstall build clean
