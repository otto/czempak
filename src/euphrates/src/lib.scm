
(define euphrates-version "EUPHRATES_VERSION_STRING_SED_PLACEHOLDER")

;;;;;;;;;;;;
;; SYNTAX ;;
;;;;;;;;;;;;

;; guile's definition
(define-syntax define-smacro
  (lambda (stx)
    (syntax-case stx ()
      ((_ (f . args) body ...)
       #'(define-macro f (lambda args body ...)))
      ((_ f gen)
       #'(define-syntax f
           (lambda (y)
             (syntax-case y ()
               ((_ . args)
                (let ((v (syntax->datum #'args)))
                  (datum->syntax y (apply gen v)))))))))))

(define-syntax letin
  (syntax-rules ()
    [(letin ((a . as) b) . ())
     (let-values [[[a . as] b]]
       (values a . as))]
    [(letin ((a . as) b) . bodies)
     (let-values [[[a . as] b]] (letin . bodies))]
    [(letin (a b) . ())
     (let [[a b]] a)]
    [(letin (a b) . bodies)
     (let [[a b]] (letin . bodies))]))

(define-syntax defloop
  (lambda (stx)
    (syntax-case stx ()
      [(defloop lambda-list . body)
       (with-syntax ([name (datum->syntax #'body 'loop)])
         #'(letrec ([name (lambda lambda-list . body)])
             name))])))

(define-syntax-rule [apploop args argv . body]
  ((defloop args . body) . argv))

(define-syntax reversed-args-buf
  (syntax-rules ()
    ((_ (x . xs) buf)
     (reversed-args-buf xs (x . buf)))
    ((_ () buf)
     buf)))
(define-syntax-rule (reversed-args . args)
  (reversed-args-buf args ()))

(define-syntax reversed-args-f-buf
  (syntax-rules ()
    ((_ f (x . xs) buf)
     (reversed-args-f-buf f xs (x . buf)))
    ((_ f () buf)
     (f . buf))))
(define-syntax-rule (reversed-args-f f . args)
  (reversed-args-f-buf f args ()))

(define-syntax reversed-lambda
  (syntax-rules ()
    [(reversed-lambda body args) (lambda [] body)]
    [(reversed-lambda body args next) (lambda (next . args) body)]
    [(reversed-lambda body args x next ...) (reversed-lambda body (x . args) next ...)]))

(define-syntax fn-start
  (syntax-rules ()
    [(fn-start args body) (reversed-lambda body () . args)]
    [(fn-start args x body ...) (fn-start (x . args) body ...)]))

(define-syntax-rule [fn . argv] (fn-start () . argv))

(define-syntax fn-list-g
  (syntax-rules ()
    [(fn-list-g lst body) body]
    [(fn-list-g lst x body ...)
     (let [[x (car lst)]]
       (fn-list-g
        (cdr lst)
        body
        ...))]))

(define-syntax-rule [fn-list . args]
  (lambda [lst] (fn-list-g lst . args)))

(define-syntax with-return
  (lambda (stx)
    (syntax-case stx ()
      [(define-job . bodies)
       (with-syntax [[return (datum->syntax #'bodies 'return)]]
         #'(call/cc (lambda [return] (begin . bodies))))])))

(define-syntax monoids*
  (syntax-rules ()
    [(monoids* x) x]
    [(monoids* op x) (op x)]
    [(monoids* a op b next-op ...)
     (monoids* (op a b) next-op ...)]))

(define-syntax monoids-r*
  (syntax-rules ()
    [(monoids-r* x) x]
    [(monoids-r* op x) (op x)]
    [(monoids-r* a op b next-op ...)
     (op a (monoids-r* b next-op ...))]))

(define-syntax monoid-r*
  (syntax-rules ()
    [(monoid-r* op a) a]
    [(monoid-r* op a b ...)
     (op a (monoid-r* op b ...))]))

(define-syntax monoid*
  (syntax-rules ()
    [(monoid* op a) a]
    [(monoid* op a b c ...)
     (monoid* op (op a b) c ...)]))

(begin-for-syntax
 (define-syntax-rule [generate-prefixed-name prefix name]
   (format-id name "~a~a" prefix (syntax->datum name))))

(define-syntax-rule (catch-any#as-pair . bodies)
  (let* ((maybe-error #f) ;; should be 'nil :D
         (result
          (catch-any
           (lambda () . bodies)
           (lambda (error)
             (set! maybe-error error)))))
    (values result maybe-error)))

(define-syntax-rule (intermezzo bind-name action . bodies)
  (let ((bind-name (begin . bodies)))
    action
    bind-name))

(define-syntax partial-apply1-helper
  (syntax-rules ()
    ((_ f buf () last) (reversed-args-f f last . buf))
    ((_ f buf (a . args) last)
     (partial-apply1-helper f (a . buf) args last))))
(define-syntax-rule (partial-apply1 f . args)
  (lambda (x)
    (partial-apply1-helper f () args x)))

(define-syntax partial-apply-helper
  (syntax-rules ()
    ((_ f buf () last) (apply f (reversed-args-f cons* last . buf)))
    ((_ f buf (a . args) last)
     (partial-apply-helper f (a . buf) args last))))
(define-syntax-rule (partial-apply f . args)
  (lambda xs
    (partial-apply-helper f () args xs)))

(define-syntax compose-under-helper
  (syntax-rules ()
    [(_ args op buf ())
     (lambda args
       (reversed-args-f op . buf))]
    [(_ args op buf (f . fs))
     (compose-under-helper
      args op
      ((apply f args) . buf)
      fs)]))
(define-syntax-rule (compose-under operation . composites)
  (compose-under-helper args operation () composites))

;; `comp` operator from clojure
(define-syntax %comp-helper
  (syntax-rules ()
    ((_ buf ())
     (compose . buf))
    ((_ buf ((x . xs) . y))
     (%comp-helper ((partial-apply1 x . xs) . buf) y))
    ((_ buf (x . y))
     (%comp-helper (x . buf) y))))
(define-syntax-rule (comp . xs)
  (%comp-helper () xs))

(define-syntax %lcomp-helper
  (syntax-rules ()
    ((_ ()) identity)
    ((_ (((x . xs)) . y))
     (lambda (input)
       ((%lcomp-helper y) ((partial-apply1 x . xs) input))))
    ((_ (((x . xs) . names) . y))
     (lambda (input)
       (let-values ((names ((partial-apply1 x . xs) input)))
         ((%lcomp-helper y) input))))
    ((_ ((x . names) . y))
     (lambda (input)
       (let-values ((names (x input)))
         ((%lcomp-helper y) input))))
    ((_ ((x) . y))
     (lambda (input)
       ((%lcomp-helper y) (x input))))))
(define-syntax-rule (lcomp . xs)
  (%lcomp-helper xs))

;; thread (->>) operator from clojure
(define-syntax-rule (appcomp x . xs)
  ((comp . xs) x))

;; extended thread (->>) operator from clojure
(define-syntax-rule (applcomp x . xs)
  ((lcomp . xs) x))

;;;;;;;;;;;;;;;;
;; SHORTHANDS ;;
;;;;;;;;;;;;;;;;

(define (string-null-or-whitespace? str)
  (let loop ((i (sub1 (string-length str))))
    (if (< i 0) #t
        (case (string-ref str i) ;; TODO: is this O(1)?
          ((#\space #\tab #\newline) (loop (sub1 i)))
          (else #f)))))

(define (list->hash-set lst)
  (let ((H (make-hash-table (length lst))))
    (let loop ((lst lst))
      (unless (null? lst)
        (hash-set! H (car lst) #t)
        (loop (cdr lst))))
    H))

(define read-list
  (case-lambda
   (() (read-list (current-input-port)))
   ((input)
    (let ((p (if (string? input) (open-input-string input)
                 input)))
      (let lp ()
        (let ((r (read p)))
          (if (eof-object? r)
              (begin
                (when (string? input)
                  (close-port p))
                (list))
              (cons r (lp)))))))))

(define list-ref-or
  (case-lambda
   ((lst ref) (list-ref-or lst ref #f))
   ((lst ref default)
    (let lp ((lst lst) (ref ref))
      (if (null? lst) default
          (if (= 0 ref)
              (car lst)
              (lp (cdr lst) (1- ref))))))))

(define (list-partition predicate lst)
  (let lp ((buf lst) (false (list)) (true (list)))
    (if (null? buf)
        (values (reverse false) (reverse true))
        (if (predicate (car buf))
            (lp (cdr buf)
                false
                (cons (car buf) true))
            (lp (cdr buf)
                (cons (car buf) false)
                true)))))

(define (list-split-on predicate lst)
  (let loop ((lst lst) (buf (list)) (ret (list)))
    (cond
     ((null? lst)
      (if (null? buf)
          (reverse ret)
          (reverse (cons (reverse buf) ret))))
     ((predicate (car lst))
      (loop (cdr lst) (list)
            (if (null? buf) ret
                (cons (reverse buf) ret))))
     (else
      (loop (cdr lst) (cons (car lst) buf) ret)))))

(define curry-if
  (case-lambda
   ((test-function then-function)
    (curry-if test-function then-function identity))
   ((test-function then-function else-function)
    (lambda (x)
      (if (test-function x) (then-function x) (else-function x))))))

;; returns list in reverse order
(define list-deduplicate
  (case-lambda
   ((lst) (list-deduplicate lst equal?))
   ((lst pred)
    (let ((H (make-hash-table (length lst))))
      (let lp ((buf lst) (mem (list)))
        (cond ((null? buf) mem)
              ((hash-ref H (car buf) #f)
               (lp (cdr buf) mem))
              (else
               (hash-set! H (car buf) #t)
               (lp (cdr buf) (cons (car buf) mem)))))))))

(define (cartesian-map function a b)
  (let lp1 ((ai a))
    (if (null? ai) (list)
        (let ((av (car ai)))
          (let lp2 ((bi b))
            (if (null? bi)
                (lp1 (cdr ai))
                (cons (function av (car bi))
                      (lp2 (cdr bi)))))))))

(define (cartesian-each function a b)
  (let lp ((ai a))
    (unless (null? ai)
      (let ((av (car ai)))
        (let lp ((bi b))
          (unless (null? bi)
            (function av (car bi))
            (lp (cdr bi)))))
      (lp (cdr ai)))))

(define [take-common-prefix a b]
  (list->string
   (let loop [[as (string->list a)]
              [bs (string->list b)]]
     (if (or (null? as)
             (null? bs))
         (list)
         (if (char=? (car as) (car bs))
             (cons (car as)
                   (loop (cdr as) (cdr bs)))
             (list))))))

(define [remove-common-prefix a b]
  (list->string
   (let loop [[as (string->list a)]
              [bs (string->list b)]]
     (cond
      ((null? as) as)
      ((null? bs) as)
      ((eq? (car as) (car bs))
       (loop (cdr as) (cdr bs)))
      (else as)))))

(define (string-trim-chars str chars-arg direction)
  (define chars (if (string? chars-arg)
                    (string->list chars-arg)
                    chars-arg))
  (define (pred c)
    (memq c chars))
  (case direction
    ((left) (string-trim str pred))
    ((right) (string-trim-right str pred))
    ((both) (string-trim-both str pred))))

(define (lines str)
  (string-split#simple str #\newline))
(define (unlines lns)
  (string-join lns "\n"))
(define (unwords lns)
  (string-join lns " "))

(define (list-intersperse element lst)
  (let lp ((buf lst))
    (if (pair? buf)
        (let ((rest (cdr buf)))
          (if (null? rest)
              buf
              (cons* (car buf)
                     element
                     (lp rest))))
        null)))

(define list-traverse
  (case-lambda
   ((lst chooser)
    (list-traverse lst #f chooser))
   ((lst default chooser)
    (let lp ((rest lst))
      (if (null? rest) default
          (let* ((head (car rest))
                 (tail (cdr rest)))
            (let-values (((continue? return) (chooser head tail)))
              (if continue?
                  (lp return)
                  return))))))))

(define (list->tree lst divider)
  (define (recur tag rest)
    (define droped (list))
    (define taken
      (let lp ((lst rest))
        (if (null? lst)
            (list)
            (let* ((x (car lst))
                   (xs (cdr lst)))
              (let-values
                  (((action d) (divider x xs)))
              (case action
                ((open)
                 (let-values
                     (((sub right) (recur x xs)))
                   (cons (append d sub)
                         (lp right))))
                ((close)
                 (set! droped xs)
                 d)
                ((turn)
                 (lp d))
                (else
                 (cons x (lp xs)))))))))

    (values taken droped))

  (let-values
      (((pre post) (recur 'root lst)))
    pre))

(define-syntax assert
  (syntax-rules ()
    ((assert test)
     (unless test
       (throw 'assertion-fail
              `(test: ,(quote test)))))
    ((assert test . printf-args)
     (unless test
       (throw 'assertion-fail
              `(test: ,(quote test))
              `(description: ,(stringf . printf-args)))))))

(define-syntax assert-norm-buf
  (syntax-rules ()
    ((_ orig buf (last-r))
     (let ((last last-r))
       (unless (reversed-args last . buf)
         (throw 'assertion-fail
                `(test: ,(quote orig))
                `(test!: ,(reversed-args-f list last . buf))))))
    ((_ orig buf (last-r) . printf-args)
     (let ((last last-r))
       (unless (reversed-args last . buf)
         (throw 'assertion-fail
                `(test: ,(quote orig))
                `(test!: ,(reversed-args-f list last . buf))
                `(description: ,(stringf . printf-args))))))
    ((_ orig buf (x-r . xs-r) . printf-args)
     (let ((x x-r))
       (assert-norm-buf orig (x . buf) xs-r . printf-args)))))

;; reduces test to normal form by hand
(define-syntax assert-norm
  (syntax-rules ()
    ((_ (x . xs) . printf-args)
     (assert-norm-buf (x . xs) () (x . xs) . printf-args))
    ((_ test . printf-args)
     (assert test . printf-args))))

(define-syntax-rule (assert-equal a b . printf-args)
  (assert-norm (equal? a b) . printf-args))

(define range
  (case-lambda
    ((start count)
     (if (> count 0)
         (cons start (range (1+ start) (1- count)))
         (list)))
    ((count)
     (range 0 count))))

(define [list-init lst]
  (take lst (1- (length lst))))

(define [normal->micro@unit s]
  (* 1000000 s))

(define [micro->nano@unit ms]
  (* 1000 ms))

(define [normal->nano@unit s]
  (micro->nano@unit (normal->micro@unit s)))

(define [nano->micro@unit ns]
  (quotient ns 1000))

(define [micro->normal@unit u]
  (quotient u 1000000))

(define [nano->normal@unit n]
  (quotient n (* 1000 1000000)))

(define [make-unique]
  "Returns procedure that returns #t if applied to itself, #f otherwise"
  (let ((euphrates-unique #f))
    (set! euphrates-unique (lambda (other)
               (eq? other euphrates-unique)))
    euphrates-unique))

(define [generic-fold first-f rest-f stop-predicate initial collection function]
  (let lp [[acc initial]
           [rest collection]]
    (if (stop-predicate rest)
        acc
        (lp (function acc (first-f rest) rest) (rest-f rest)))))

(define-smacro [generic-fold-macro first-f
                                  rest-f
                                  stop-predicate
                                  initial
                                  collection
                                  . body]
  `(generic-fold ,first-f ,rest-f ,stop-predicate ,initial ,collection
                   (lambda [acc x rest] ,@ body)))

(define [list-fold initial lst function]
  (generic-fold car cdr null? initial lst
                (lambda [acc x rest] (function acc x))))

(define [list-fold/rest initial lst function]
  (generic-fold car cdr null? initial lst function))

(define-smacro [lfold initial lst . body]
  `(generic-fold-macro car cdr null? ,initial ,lst ,@ body))

(define [simplify-posix-path path]
  (let* [[splits (string-split#simple path #\/)]
         [norm
          (let lp [[buf (list)] [rest splits]]
            (if (null? rest) (reverse buf)
                (let [[cur (car rest)]]
                  (if (string=? cur ".")
                      (lp buf (cdr rest))
                      (if (string=? cur "..")
                          (lp (cond
                               ((null? buf) (cons cur buf))
                               ((string=? "" (car buf)) buf) ;; starts with root
                               ((string=? ".." (car buf)) (cons cur buf)) ;; starts with ..
                               (else (cdr buf)))
                              (cdr rest))
                          (lp (cons cur buf) (cdr rest)))))))]
         [ret (string-join norm "/")]]
    ret))

(define (absolute-posix-path? path)
  (and (string? path)
       (char=? (string-ref path 0) #\/)))

(define [append-posix-path2 a b]
  (if (= (string-length a) 0)
      b
      (let ((b
             (if (absolute-posix-path? b)
                 (let ((cl (remove-common-prefix b a)))
                   (if (equal? cl b)
                       (throw 'append-posix-path-disjoint `(args: ,a ,b))
                       cl))
                 b)))
        (if (char=? #\/ (string-ref a (1- (string-length a))))
            (string-append a b)
            (string-append a "/" b)))))

(define [append-posix-path . paths]
  (list-fold "" paths append-posix-path2))

(define (path-without-extension str)
  (let ((index (string-index-right str #\.)))
    (string-take str index)))

(define (path-replace-extension str new-ext)
  (let ((stripped (path-without-extension str)))
    (string-append stripped new-ext)))

;; specified by RFC 4648
(define base64#alphabet
  #(#\A #\B #\C #\D #\E #\F #\G #\H
    #\I #\J #\K #\L #\M #\N #\O #\P
    #\Q #\R #\S #\T #\U #\V #\W #\X
    #\Y #\Z #\a #\b #\c #\d #\e #\f
    #\g #\h #\i #\j #\k #\l #\m #\n
    #\o #\p #\q #\r #\s #\t #\u #\v
    #\w #\x #\y #\z #\0 #\1 #\2 #\3
    #\4 #\5 #\6 #\7 #\8 #\9 #\- #\_))

(define alphanum#alphabet
  #(#\0 #\1 #\2 #\3 #\4 #\5 #\6 #\7 #\8 #\9
    #\a #\b #\c #\d #\e #\f #\g #\h #\i #\j
    #\k #\l #\m #\n #\o #\p #\q #\r #\s #\t
    #\u #\v #\w #\x #\y #\z #\A #\B #\C #\D
    #\E #\F #\G #\H #\I #\J #\K #\L #\M #\N
    #\O #\P #\Q #\R #\S #\T #\U #\V #\W #\X
    #\Y #\Z))

(define alpha#alphabet
  #(#\a #\b #\c #\d #\e #\f #\g #\h #\i #\j
    #\k #\l #\m #\n #\o #\p #\q #\r #\s #\t
    #\u #\v #\w #\x #\y #\z #\A #\B #\C #\D
    #\E #\F #\G #\H #\I #\J #\K #\L #\M #\N
    #\O #\P #\Q #\R #\S #\T #\U #\V #\W #\X
    #\Y #\Z))

(define printable#alphabet
  #(#\0 #\1 #\2 #\3 #\4 #\5 #\6 #\7 #\8 #\9
    #\a #\b #\c #\d #\e #\f #\g #\h #\i #\j
    #\k #\l #\m #\n #\o #\p #\q #\r #\s #\t
    #\u #\v #\w #\x #\y #\z #\A #\B #\C #\D
    #\E #\F #\G #\H #\I #\J #\K #\L #\M #\N
    #\O #\P #\Q #\R #\S #\T #\U #\V #\W #\X
    #\Y #\Z #\+ #\/ #\@ #\! #\& #\* #\= #\?
    #\( #\) #\- #\% #\# #\, #\. #\^ #\' #\[
    #\] #\{ #\} #\; #\: #\\ #\< #\> #\" #\$))

(define (random-choice len alphabet#vector)
  (let ((size (vector-length alphabet#vector)))
    (let loop ((len len) (buf (list)))
      (if (<= len 0) buf
          (loop (sub1 len)
                (cons (vector-ref alphabet#vector (big-random-int size))
                      buf))))))

;; TODO: make something safe instead?
(define (make-temporary-filename)
  (let* ((s (list->string (random-choice 10 alphanum#alphabet))))
    (string-append "/tmp/euphrates-temp-" s)))

(define (current-source-info->string info)
  (let* ((linei (assq 'line info))
         (columni (assq 'column info))
         (filenamei (assq 'filename info))
         (cwd (string-append (getcwd) "/"))

         (line (or (and linei (string-append (~a (cdr linei)) ":"))
                   ""))
         (column (or (and columni (string-append (~a (cdr columni)) ":"))
                     ""))
         (filename
          (or (and filenamei
                   (string-append
                    (remove-common-prefix (cdr filenamei) cwd) ":"))
              ""))

         (alli (string-append filename line column)))
    (or (and alli (string-append alli " "))
        "")))

(define [stringf fmt . args]
  (with-output-to-string
    (lambda []
      (apply printf (cons* fmt args)))))

(define dprint#p-default printf)
(define dprint#p
  (make-parameter dprint#p-default))
(define dprint
  (lambda args
    (apply (dprint#p) args)))
(define [dprintln fmt . args]
  (apply dprint (cons* (string-append fmt "\n") args)))

(define global-debug-mode-filter (make-parameter #f))

(define [debug fmt . args]
  (let [[p (global-debug-mode-filter)]]
    (when (or (not p) (p fmt args))
      (parameterize ((current-output-port (current-error-port)))
        (apply printf (cons* (string-append fmt "\n") args))))))

(define-syntax-rule (with-ignore-errors! . bodies)
  (catch-any
   (lambda _ . bodies)
   (lambda errors
     (debug "~aerror: ~s"
            (current-source-info->string
             (get-current-source-info))
            errors))))

;; Logs computations
(define [dom-print name result x cont]
  (dprint "(~a = ~a = ~a)\n" name x result)
  (cont x))

(define [port-redirect from to]
  "Redirect from `from' to `to' byte by byte, until `eof-object?'
   Returns count of written bytes

   type ::= port -> port -> int
  "
  (let lp [[count 0]]
    (let [[byte (get-u8 from)]]
      (if (eof-object? byte)
          count
          (begin
            (put-u8 to byte)
            (lp (1+ count)))))))

(define-syntax-rule (cons! x lst)
  (set! lst (cons x lst)))

; memoized constant function
(define-syntax-rule (memconst x)
  (let ((memory #f)
        (evaled? #f))
    (lambda argv
      (unless evaled?
        (set! evaled? #t)
        (set! memory x))
      memory)))

; memoized value
(define-syntax-rule (memvalue x)
  (let ((memory #f)
        (evaled? #f))
    (case-lambda
     (()
      (unless evaled?
        (set! evaled? #t)
        (set! memory x))
      memory)
     ((type . args)
      (case type
        ((check) evaled?)
        ((or) (or memory (car args)))
        (else (throw 'unknown-memoize-command type memory evaled?)))))))

(define-syntax-rule (effectvalue value . effects)
  (let ((evaled? #f))
    (case-lambda
     (() (unless evaled? (set! evaled? #t) . effects) value)
     ((type . args)
      (case type
        ((check) evaled?)
        ((or) (or (and evaled? value) (car args)))
        ((value) value)
        (else (throw 'unknown-effectvalue-command type evaled?)))))))

(define (replicate n x)
  (if (= 0 n)
      (list)
      (cons x (replicate (1- n) x))))

;;; thread abstractions

(define (dynamic-thread-get-delay-procedure#default)
  (let ((sleep (dynamic-thread-sleep-p))
        (timeout (dynamic-thread-wait-delay#us-p)))
    (lambda ()
      (sleep timeout))))

;; NOTE: don't use -p parameters unless parameterizing!

(define dynamic-thread-spawn-p (make-parameter sys-thread-spawn))
(define (dynamic-thread-spawn thunk) ((dynamic-thread-spawn-p) thunk))

(define dynamic-thread-cancel-p (make-parameter sys-thread-cancel))
(define (dynamic-thread-cancel thunk) ((dynamic-thread-cancel-p) thunk))

;; for critical zones
(define dynamic-thread-disable-cancel-p
  (make-parameter sys-thread-disable-cancel))
(define (dynamic-thread-disable-cancel)
  ((dynamic-thread-disable-cancel-p)))
(define dynamic-thread-enable-cancel-p
  (make-parameter sys-thread-enable-cancel))
(define (dynamic-thread-enable-cancel)
  ((dynamic-thread-enable-cancel-p)))

;; This yield should also be called by thread manager while sleeping
(define dynamic-thread-yield-p (make-parameter (lambda () 0)))
(define (dynamic-thread-yield) ((dynamic-thread-yield-p)))

(define dynamic-thread-wait-delay#us-p
  (make-parameter 1000))

(define dynamic-thread-sleep-p (make-parameter sys-thread-sleep))
(define [dynamic-thread-sleep micro-seconds]
  ((dynamic-thread-sleep-p) micro-seconds))

(define dynamic-thread-get-delay-procedure-p
  (make-parameter dynamic-thread-get-delay-procedure#default))
(define (dynamic-thread-get-delay-procedure)
  ((dynamic-thread-get-delay-procedure-p)))

(define (dynamic-thread-get-yield-procedure)
  (dynamic-thread-yield-p))

(define dynamic-thread-cancel-tag
  'euphrates-dynamic-thread-cancelled)

;; NOTE ON USING MUTEXES AND CRITICAL ZONES
;; Critical zones must not evaluate non-local
;; jumps, such as exceptions, or yield!
;; np-thread and others rely on this.
;; Use critical zones where it is clear that
;; code doesn't have jumps because they
;; cannot be cancelled, and they are
;; faster than locks, for some contexts,
;; and are easier to use
;; Otherwise, use locks
;; When parameterizing locks/critical-zones
;; make sure that application uses compatible
;; set of thread-model/blocking-methods pairs
;; For example, if application uses posix threads
;; along with np-thread's, they you should use
;; uni-spinlocks which are the most strict one

(define (dynamic-thread-mutex-make)
  ((dynamic-thread-mutex-make-p)))
(define (dynamic-thread-mutex-lock! mut)
  ((dynamic-thread-mutex-lock!-p) mut))
(define (dynamic-thread-mutex-unlock! mut)
  ((dynamic-thread-mutex-unlock!-p) mut))

(define (dynamic-thread-critical-make#default)
  (let* ((mut (dynamic-thread-mutex-make))
         (lock-func (dynamic-thread-mutex-lock!-p))
         (unlock-func (dynamic-thread-mutex-unlock!-p))
         (lock (lambda () (lock-func mut)))
         (unlock (lambda () (unlock-func mut))))
    (lambda (thunk)
      (dynamic-thread-disable-cancel)
      (lock)
      (let ((ret (thunk)))
        (unlock)
        (dynamic-thread-enable-cancel)
        ret))))

(define dynamic-thread-critical-make-p
  (make-parameter dynamic-thread-critical-make#default))
(define (dynamic-thread-critical-make)
  ((dynamic-thread-critical-make-p)))

;; Universal spinlock
;; Works for any thread model
;; Very wasteful
(define-values
    (make-uni-spinlock
     uni-spinlock-lock!
     uni-spinlock-unlock!
     make-uni-spinlock-critical)

  (let* ((make (lambda () (make-atomic-box #f)))

         (lock
          (lambda (o)
            (let ((yield (dynamic-thread-get-yield-procedure)))
              (let lp ()
                (unless (atomic-box-compare-and-set!
                         o #f #t)
                  (yield)
                  (lp))))))

         (unlock
          (lambda (o)
            (atomic-box-set! o #f)))

         (critical
          (lambda ()
            (let ((box (make)))
              (lambda (thunk)
                (dynamic-thread-disable-cancel)
                (lock box)
                (let ((ret (thunk)))
                  (unlock box)
                  (dynamic-thread-enable-cancel)
                  ret))))))
    (values make lock unlock critical)))

(define-syntax-rule (with-critical critical-func . bodies)
  (critical-func
   (lambda [] . bodies)))


(define-syntax-rule [sleep-until condi . body]
  (let ((sleep (dynamic-thread-get-delay-procedure)))
    (do ()
        (condi)
      (sleep)
      . body)))

(define (dynamic-thread-async-thunk thunk)
  (let ((results #f)
        (status #f)) ;; \in { #f, 'ok, 'fail }

    (dynamic-thread-spawn
     (lambda ()
       (catch-any
        (lambda ()
          (call-with-values thunk
            (lambda vals
              (set! results vals)
              (set! status 'ok))))
        (lambda errors
          (set! results errors)
          (set! status 'fail)))))

    (lambda ()
      (sleep-until status)
      (when (eq? 'fail status)
        (throw 'dynamic-thread-run-async-failed results))
      (apply values results))))

(define-syntax-rule (dynamic-thread-async . bodies)
  (dynamic-thread-async-thunk (lambda () . bodies)))

;; Like uni-spinlock but use arbitary variables as lock target
;; and do sleep when wait
(define-values
  [universal-lockr! universal-unlockr!]
  (let [[critical (make-uni-spinlock-critical)]
        [h (make-hash-table)]]
    (values
     (lambda [resource]
       (let ((sleep (dynamic-thread-get-delay-procedure)))
         (let lp []
           (when
               (with-critical
                critical
                (let [[r (hash-ref h resource #f)]]
                  (if r
                      #t
                      (begin
                        (hash-set! h resource #t)
                        #f))))
             (sleep)
             (lp)))))
     (lambda [resource]
       (with-critical
        critical
        (hash-set! h resource #f))))))

(define [universal-usleep micro-seconds]
  (let* [[nano-seconds (micro->nano@unit micro-seconds)]
         [start-time (time-get-monotonic-nanoseconds-timestamp)]
         [end-time (+ start-time nano-seconds)]
         [sleep-rate (dynamic-thread-wait-delay#us-p)]
         [yield (dynamic-thread-get-yield-procedure)]]
    (let lp []
      (let [[t (time-get-monotonic-nanoseconds-timestamp)]]
        (unless (> t end-time)
          (let [[s (min sleep-rate
                        (nano->micro@unit (- end-time t)))]]
            (yield)
            (usleep s)
            (lp)))))))

;;;;;;;;;;;;
;; MONADS ;;
;;;;;;;;;;;;

(define-syntax monadic-bare-handle-tags
  (syntax-rules ()
    ((monadic-bare-handle-tags ())
     (list))
    ((monadic-bare-handle-tags . tags)
     (list . tags))))

;; like do syntax in haskell
(define-syntax monadic-bare
  (syntax-rules ()
    [(_ f ((a . as) b . tags) . ())
     (let-values
         (((r-x r-cont qvar qval qtags last?)
           (f (memconst (call-with-values (lambda _ b) (lambda x x)))
              (lambda args (apply values args))
              (quote (a . as))
              (quote b)
              (monadic-bare-handle-tags . tags)
              #t)))
       (r-cont (r-x)))]
    [(_ f ((a . as) b . tags) . bodies)
     (let-values
         (((r-x r-cont qvar qval qtags last?)
           (f (memconst (call-with-values (lambda _ b) (lambda x x)))
              (lambda [k]
                (apply
                 (lambda [a . as]
                   (monadic-bare f . bodies))
                 k))
              (quote (a . as))
              (quote b)
              (monadic-bare-handle-tags . tags)
              #f)))
       (r-cont (r-x)))]
    [(_ f (a b . tags) . ())
     (let-values
         (((r-x r-cont qvar qval qtags last?)
           (f (memconst b)
              identity
              (quote a)
              (quote b)
              (monadic-bare-handle-tags . tags)
              #t)))
       (r-cont (r-x)))]
    [(_ f (a b . tags) . bodies)
     (let-values
         (((r-x r-cont qvar qval qtags last?)
           (f (memconst b)
              (lambda [a]
                (monadic-bare f . bodies))
              (quote a)
              (quote b)
              (monadic-bare-handle-tags . tags)
              #f)))
       (r-cont (r-x)))]))

(define monadic-global-parameter (make-parameter #f))
(define-syntax-rule [monadic-parameterize f . body]
  (parameterize [[monadic-global-parameter f]]
    (begin . body)))

(define-syntax-rule [with-monadic-left f . body]
  (let ((current-monad (monadic-global-parameter)))
    (let ((new-monad
           (lambda (old-monad old-monad-quoted)
             (let ((applied (if current-monad
                                (current-monad old-monad old-monad-quoted)
                                old-monad)))
               (compose f applied)))))
      (parameterize [[monadic-global-parameter new-monad]]
        (begin . body)))))
(define-syntax-rule [with-monadic-right f . body]
  (let ((current-monad (monadic-global-parameter)))
    (let ((new-monad
           (lambda (old-monad old-monad-quoted)
             (let ((applied (if current-monad
                                (current-monad old-monad old-monad-quoted)
                                old-monad)))
               (compose applied f)))))
      (parameterize [[monadic-global-parameter new-monad]]
        (begin . body)))))

;; with parameterization
(define-syntax-rule [monadic fexpr . argv]
  (let* [[p (monadic-global-parameter)]
         [f fexpr]]
    (if p
        (monadic-bare (p f (quote fexpr)) . argv)
        (monadic-bare f . argv))))

(define-syntax-rule [monadic-id . argv]
  (monadic identity-monad . argv))

(define (monad-arg#lazy monad-input)
  (first monad-input))
(define (monad-arg monad-input)
  ((monad-arg#lazy monad-input))) ;; NOTE: evaluates it right away
(define (monad-cont monad-input)
  (second monad-input))
(define (monad-qvar monad-input)
  (third monad-input))
(define (monad-qval monad-input)
  (fourth monad-input))
(define (monad-qtags monad-input)
  (fifth monad-input))
(define (monad-last? monad-input)
  (sixth monad-input))

(define-syntax-rule (monad-cret monad-input arg cont)
  (values (memconst arg)
          cont
          (monad-qvar monad-input)
          (monad-qval monad-input)
          (monad-qtags monad-input)
          (monad-last? monad-input)))
(define-syntax-rule (monad-ret monad-input arg)
  (values (memconst arg)
          (monad-cont monad-input)
          (monad-qvar monad-input)
          (monad-qval monad-input)
          (monad-qtags monad-input)
          (monad-last? monad-input)))
(define-syntax-rule (monad-ret-id monad-input)
  (apply values monad-input))

(define (monad-handle-multiple monad-input arg)
  (let* ((qvar (monad-qvar monad-input))
         (len (if (list? qvar) (length qvar) 1)))
    (if (< len 2)
        arg
        (replicate len (arg)))))
(define (monad-replicate-multiple monad-input arg)
  (let* ((qvar (monad-qvar monad-input))
         (len (if (list? qvar) (length qvar) 1)))
    (if (< len 2)
        arg
        (replicate len arg))))

(define (except-monad)
  (let ((exceptions (list)))
    (lambda monad-input
      (if (monad-last? monad-input)
          (monad-ret monad-input
                     (monad-handle-multiple
                      monad-input
                     (if (null? exceptions)
                         (monad-arg monad-input)
                         (apply throw 'except-monad exceptions))))
          (if (or (null? exceptions)
                  (memq 'always (monad-qtags monad-input)))
              (monad-ret monad-input
                         (catch-any
                          (monad-arg#lazy monad-input)
                          (lambda args
                            (cons! args exceptions)
                            (monad-replicate-multiple
                             monad-input
                             'monad-except-default))))
              (monad-ret monad-input
                         (monad-handle-multiple
                          monad-input
                          'monad-except-default)))))))

(define log-monad
  (lambda monad-input
    (dprint "(~a = ~a = ~a)\n"
            (monad-qvar monad-input)
            (monad-arg monad-input)
            (monad-qval monad-input))
    (monad-ret-id monad-input)))

(define identity-monad
  (lambda monad-input (apply values monad-input)))

(define (maybe-monad predicate)
  (lambda monad-input
    (let ((arg (monad-arg monad-input)))
      (if (predicate arg)
          (monad-cret monad-input arg identity)
          (monad-ret  monad-input arg)))))

(define lazy-monad
  (lambda monad-input
    (let* ((qvar (monad-qvar monad-input))
           (len (if (list? qvar) (length qvar) 1))
           (single? (< len 2))
           (result
            (if (memq 'async (monad-qtags monad-input))
                (dynamic-thread-async (monad-arg monad-input))
                (monad-arg#lazy monad-input)))
           (choose
            (lambda (i)
              (memconst
                (list-ref (result) i))))
           (return
            (if single? result
                (map choose (range len)))))
      (monad-ret monad-input return))))

;; Replaces expressions by different ones based on associted tags
;; Can be used for deriving many less general monads, like lazy-monad or filter-monad
(define (replace-monad test/replace-procedure)
  (lambda monad-input
    (let* ((tags (monad-qtags monad-input))
           (arg#lazy (monad-arg#lazy monad-input)))
      (apply values
             (cons (test/replace-procedure tags arg#lazy)
                   (cdr monad-input))))))

;; Skips evaluation based on given predicate
;; NOTE: don't use on multiple-values!
(define (filter-monad test-any)
  (replace-monad
   (lambda (tags arg#lazy)
     (if (or-map test-any tags)
         (lambda () 'filter-monad-skipped-evaluation)
         arg#lazy))))

;;;;;;;;;;;;;
;; BRACKET ;;
;;;;;;;;;;;;;

(define call-with-finally#return-tag
  'euphrates-call-with-finally#return-error)

;; Applies `return' function to expr.
;; `return' is a call/cc function, but it ensures that `finally' is called.
;; Also, if exception is raised, `finally' executes.
;; `finally' executes only once! No matter how many exits are done.
;; Composable, so that if bottom one calls `return', all `finally's are going to be called in correct order.
;; Returns evaluated expr or re-throws an exception
;;
;; This is different from `dynamic-wind'
;; because it executes `finally' before returning the control
;; and it does not catch any non local jumps except the `return' and throws
;;
;; expr ::= ((Any... -> Any) -> Any)
;; finally ::= (-> Any)
(define call-with-finally#return
  (let [[dynamic-stack (make-parameter (list))]]
    (lambda [expr finally]
      (let* [[err #f] [normal? #t]
             [finally-executed? #f]
             [finally-wraped
              (lambda args
                (unless finally-executed?
                  (set! finally-executed? #t)
                  (apply finally args)))]]

        (call-with-values
            (lambda ()
              (catch-any
               (lambda []
                 (call/cc
                  (lambda [k]
                    (parameterize
                        [[dynamic-stack
                          (cons (cons k finally-wraped) (dynamic-stack))]]
                      (expr (lambda argv
                              (set! normal? #f)

                              (let lp [[st (dynamic-stack)]]
                                (unless (null? st)
                                  (let [[p (car st)]]
                                    ((cdr p))
                                    (when (not (eq? (car p) k))
                                      (lp (cdr st))))))

                              (apply k argv)
                              ))))))
               (lambda args
                 (set! err args))))
          (lambda ret
            (when normal? (finally-wraped))
            (when err (apply throw err))
            (apply values ret)))))))

;; Runs finally even if exception was thrown (even on thread cancel)
;; expr ::= (-> Any)
;; finally ::= (-> Any)
(define [call-with-finally expr finally]
  (let ((err #f))
    (catch-any
     expr
     (lambda errors
       (set! err errors)))
    (finally)
    (when err
      (apply throw err))))

;;;;;;;;;;;;;;;;
;; FILESYSTEM ;;
;;;;;;;;;;;;;;;;

(define read-all-port
  (case-lambda
    ((readf port)
     "`readf' is usually `read-char' or `read-byte'"
     (let loop ((result '()) (chr (readf port)))
       (if (eof-object? chr)
           (list->string (reverse result))
           (loop (cons chr result) (readf port)))))
    ((port)
     (read-all-port read-char port))))

(define [read-string-file path]
  (let* [
   [in (open-file path "r")]
   [text (read-all-port read-char in)]
   (go (close-port in))]
   text))

(define [write-string-file path data]
  (let* [[out (open-file path "w")]
         [re (display data out)]
         (go (close-port out))]
    re))

(define [append-string-file path data]
  (let* [[out (open-file path "a")]
         [re (display data out)]
         (go (close-port out))]
    re))

(define [path-redirect newbase path]
  (let [[oldbase (take-common-prefix newbase path)]]
    (string-append newbase
                   (substring path
                              (string-length oldbase)))))

(define [path-rebase newbase path]
  (let [[oldbase (take-common-prefix newbase path)]]
    (if (string-null? oldbase)
        #f
        (string-append newbase
                       (substring path
                                  (string-length oldbase))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; NON PREEMPTIVE THREADS ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define-rec np-thread-obj
  continuation
  cancel-scheduled?
  cancel-enabled?
  )

;; Disables critical zones because in non-interruptible mode
;; user can assure atomicity by themself
;; Locks still work as previusly,
;; but implementation must be changed,
;; because system mutexes will not allow to do yield
;; while waiting on mutex.
(define (np-thread-parameterize-env make-critical thunk)

  (define (make-np-thread-obj thunk)
    (np-thread-obj thunk #f #t))

  (define thread-queue (make-queue 16))
  (define current-thread #f)
  (define critical (make-critical))

  (define (np-thread-list-add th)
    (with-critical
     critical
     (queue-push! thread-queue th)))

  (define (np-thread-list-switch)
    (with-critical
     critical
     (let loop ((head (queue-pop! thread-queue #f)))
       (if (not head) 'np-thread-empty-list
           (if (and (np-thread-obj-cancel-scheduled? head)
                    (np-thread-obj-cancel-enabled? head))
               (begin
                 (loop (queue-pop! thread-queue #f)))
               (begin
                 (set! current-thread head)
                 head))))))

  (define start-point #f)

  (define [np-thread-end]
    (let [[p (np-thread-list-switch)]]
      (if (eq? p 'np-thread-empty-list)
          (start-point)
          (begin
            ((np-thread-obj-continuation p))
            (np-thread-end)))))

  (define [np-thread-yield]
    (let [[me current-thread]]
      (when (and (np-thread-obj-cancel-scheduled? me)
                 (np-thread-obj-cancel-enabled? me))
        (throw dynamic-thread-cancel-tag))

      (call/cc
       (lambda (k)
         (set-np-thread-obj-continuation! me k)
         (np-thread-list-add me)
         (np-thread-end)))))

  (define [np-thread-fork thunk]
    (let ((first? #t)
          (ret #f))
      (call/cc
       (lambda (k)
         (set! ret (make-np-thread-obj k))
         (np-thread-list-add ret)))
      (unless first?
        (thunk)
        (np-thread-end))
      (set! first? #f)
      ret))

  (define [np-thread-run! thunk]
    (call/cc
     (lambda [k]
       (set! start-point k)
       (set! current-thread (make-np-thread-obj thunk))
       (thunk)
       (np-thread-end))))

  ;; Terminates np-thread
  ;; If no arguments given, current thread will be terminated
  ;; But if thread is provided, it will be removed from thread list (equivalent to termination if that thread is not the current one)
  ;; Therefore, don't provide current thread as argument unless you really mean to
  (define np-thread-cancel!#unsafe
    (case-lambda
      [[] (np-thread-end)]
      [[chosen] 0]))

  (define (np-thread-cancel! chosen)
    (set-np-thread-obj-cancel-scheduled?! chosen #t)
    (when (np-thread-obj-cancel-enabled? chosen)
      (np-thread-cancel!#unsafe chosen)))

  (define (np-thread-make-critical)
    (lambda (fn)
      (let* [[me current-thread]]
        (set-np-thread-obj-cancel-enabled?! me #f)
        (fn) ;; NOTE: must not evaluate non-local jumps
        (set-np-thread-obj-cancel-enabled?! me #t)
        (np-thread-yield)
        )))

  (define [np-thread-disable-cancel]
    (let ((me current-thread))
      (set-np-thread-obj-cancel-enabled?! me #f)))
  (define [np-thread-enable-cancel]
    (let ((me current-thread))
      (set-np-thread-obj-cancel-enabled?! me #t)))

  (parameterize ((dynamic-thread-spawn-p np-thread-fork)
                 (dynamic-thread-cancel-p np-thread-cancel!)
                 (dynamic-thread-disable-cancel-p np-thread-disable-cancel)
                 (dynamic-thread-enable-cancel-p np-thread-enable-cancel)
                 (dynamic-thread-yield-p np-thread-yield)
                 (dynamic-thread-sleep-p universal-usleep)
                 (dynamic-thread-mutex-make-p make-unique)
                 (dynamic-thread-mutex-lock!-p universal-lockr!)
                 (dynamic-thread-mutex-unlock!-p universal-unlockr!)
                 (dynamic-thread-critical-make-p np-thread-make-critical))
    (np-thread-run! thunk)))

(define-syntax-rule (with-np-thread-env#non-interruptible . bodies)
  (np-thread-parameterize-env (lambda () (lambda (fn) (fn)))
                              (lambda () . bodies)))

;;;;;;;;;;;;;;;
;; PROCESSES ;;
;;;;;;;;;;;;;;;

;; if #f then (current-input-port)
(define comprocess-stdout
  (make-parameter #f))
;; if #f then (current-error-port)
(define comprocess-stderr
  (make-parameter #f))

(define run-comprocess#p
  (make-parameter run-comprocess#p-default))
(define run-comprocess
  (lambda args
    (apply (run-comprocess#p) args)))

(define kill-comprocess#p
  (make-parameter kill-comprocess#p-default))
(define kill-comprocess
  (lambda args
    (apply (kill-comprocess#p) args)))

(define [kill-comprocess-with-timeout p timeout]
  (unless (comprocess-exited? p)
    (kill-comprocess p #f)
    (unless (comprocess-exited? p)
      (dynamic-thread-spawn
       (lambda []
         (dynamic-thread-sleep timeout)
         (unless (comprocess-exited? p)
           (kill-comprocess p #t)))))))

;;;;;;;;;;;;;;;;;;;;;;;
;; GENERIC FUNCTIONS ;;
;;;;;;;;;;;;;;;;;;;;;;;

(begin-for-syntax
 (define-syntax-rule [generate-add-name name]
   (generate-prefixed-name 'gfunc/instantiate- name))

 (define-syntax-rule [generate-param-name name]
   (generate-prefixed-name 'gfunc/parameterize- name)))

(define [check-list-contract check-list args]
  (or (not check-list)
      (and (= (length check-list) (length args))
           (fold (lambda [p x c] (and c (p x))) #t check-list args))))

(define-syntax gfunc/define
  (lambda (stx)
    (syntax-case stx ()
      [[gfunc/define name]
       (with-syntax ([add-name (generate-add-name #'name)]
                     [param-name (generate-param-name #'name)])
         #'(define-values [name add-name param-name]
             (let [[internal-list (make-parameter '())]
                   [critical (make-uni-spinlock-critical)]]
               (values
                (lambda args
                  (let [[m (find-first (lambda [p] (check-list-contract (car p) args)) (internal-list))]]
                    (if m
                        (apply (cdr m) args)
                        (throw 'gfunc-no-instance-found
                               (string-append "No gfunc instance of "
                                              (symbol->string (syntax->datum #'name))
                                              " accepts required arguments")))))
                (lambda [args func]
                  (with-critical
                   critical
                   (set! internal-list
                     (make-parameter (append (internal-list)
                                             (list (cons args
                                                         func)))))))
                (lambda [args func body]
                  (let [[new-list (cons (cons args func) (internal-list))]]
                    (parameterize [[internal-list new-list]]
                      (body))))))))])))

(define-syntax gfunc/parameterize
  (lambda (stx)
    (syntax-case stx ()
      [[gfunc/parameterize name check-list func . body]
       (with-syntax [[param-name (generate-param-name #'name)]]
         #'(param-name check-list func (lambda [] . body)))])))

(define-syntax gfunc/instance
  (lambda (stx)
    (syntax-case stx ()
      [[gfunc/instance name check-list func]
       (with-syntax [[add-name (generate-add-name #'name)]]
         #'(add-name (list . check-list) func))])))


;;;;;;;;;;;;;
;; PACKAGE ;;
;;;;;;;;;;;;;

;; This is for ad hoc polymorphism
;; Faster than gfunc, but also more limited
;; Similar to SML parametric modules

(define-syntax with-svars-helper
  (syntax-rules []
    [[_ dict buf [] body]
     (lambda dict
       (let buf body))]
    [[_ dict buf [name . names] body]
     (with-svars-helper
      dict
      ((name
        (let [[z (assq (quote name) dict)]]
          (if z (cdr z) name))) . buf)
      names
      body)]))

(define-syntax-rule [with-svars names body]
  (with-svars-helper dd () names body))

(define-syntax use-svars-helper
  (syntax-rules []
    [[_ [] [] f] (f)]
    [[_ buf [] f] (f . buf)]
    [[_ buf [[name value] . names] f]
     (use-svars-helper
      ((cons (quote name) value) . buf)
      names
      f)]))

(define-syntax-rule [use-svars f . renames]
  (use-svars-helper [] renames f))

(define-syntax make-static-package-helper
  (syntax-rules []
    [[_ hh buf []]
     (let [[hh (make-hash-table)]]
       (begin . buf)
       hh)]
    [[_ hh buf [[name value] . definitions]]
     (make-static-package-helper
      hh
      [(hash-set! hh
                  (quote name)
                  value) . buf]
      definitions)]))

(define-syntax-rule [make-static-package definitions]
  (make-static-package-helper hh [] definitions))

(define-syntax-rule [make-package inputs definitions]
  (with-svars
   inputs
   (make-static-package definitions)))

(define-syntax with-package-helper
  (syntax-rules []
    [[_ inst [] body] body]
    [[_ inst [name . names] body]
     (let [[name (hash-ref inst (quote name))]]
       (with-package-helper
        inst
        names
        body))]))

(define-syntax with-package-renames-helper-pre
  (syntax-rules []
    [[_ [package . renames]]
     (use-svars package . renames)]
    [[_ package]
     (package)]))

(define-syntax-rule [with-package package-spec names body]
  (let [[inst (with-package-renames-helper-pre package-spec)]]
    (with-package-helper inst names body)))

;;;;;;;;;;;;;;;;;;;;
;; HASHED RECORDS ;;
;;;;;;;;;;;;;;;;;;;;

(define [hash->mdict h]
  (let [[unique (make-unique)]]
    (case-lambda
      [[] h]
      [[key]
       (let [[g (hash-ref h key unique)]]
         (if (unique g)
             (throw 'mdict-key-not-found key h)
             g))]
      [[key value]
       (let* [[new (make-hash-table)]]
         (hash-table-foreach
          h
          (lambda (key value)
            (hash-set! new key value)))
         (hash-set! new key value)
         (hash->mdict new))])))

(define [alist->mdict alist]
  (hash->mdict (alist->hash-table alist)))

(define-syntax mdict-c
  (syntax-rules ()
    [(mdict-c carry) (alist->mdict carry)]
    [(mdict-c carry key value . rest)
     (mdict-c (cons (cons key value) carry) . rest)]))

(define-syntax-rule [mdict . entries]
  (mdict-c '() . entries))

(define [mdict-has? h-func key]
  (let [[h (h-func)]]
    (hash-get-handle h key)))

(define [mdict-set! h-func key value]
  (let [[h (h-func)]]
    (hash-set! h key value)))

(define [mdict->alist h-func]
  (let [[h (h-func)]]
    (hash-table->alist h)))

(define [mdict-keys h-func]
  (map car (mdict->alist h-func)))

;;;;;;;;;;;;;
;; SCRIPTS ;;
;;;;;;;;;;;;;

(define (shell-check-status p)
  (unless (equal? 0 (comprocess-status p))
    (throw 'shell-process-failed `(cmd: ,(comprocess-command p)) p)))

(define (shell-inputs-to-comprocess-args inputs)
  ;; assert((and (list? inputs) (not (null? inputs))))
  (if (= 1 (length inputs))
      (let ((cmd (car inputs)))
        (if (list? cmd)
            (values cmd cmd)
            (values (list "/bin/sh" "-c" cmd) cmd)))
      (let ((cmd (apply stringf inputs)))
        (values
         (list "/bin/sh" "-c" cmd)
         cmd))))

(define [sh-async-no-log . inputs]
  (apply run-comprocess
         (shell-inputs-to-comprocess-args inputs)))

(define (sh-async . inputs)
  (monadic-id
   ((args cmd) (shell-inputs-to-comprocess-args inputs))
   (ret (apply run-comprocess args))
   (do (debug "> ~a" cmd) `(sh-cmd ,cmd) 'sh-log)
   (do ret)))

(define [sh . inputs]
  (monadic-id
   (p (apply sh-async inputs))
   (do (sleep-until (comprocess-exited? p)))
   (do (shell-check-status p))
   (do (kill-comprocess-with-timeout p (normal->micro@unit 1/2))
       'always 'sh-kill-on-error p)
   (do p)))

(define [sh-re . inputs]
  (string-trim-chars (with-output-to-string (lambda _ (apply sh inputs))) "\n \t" 'both))

(define-syntax-rule (with-no-shell-log . bodies)
  (with-monadic-right
   (filter-monad
    (lambda (tag)
      (case tag
        ((sh-log sh-return-log) #t)
        (else #f))))
   . bodies))

(define (system-re command)
  "Like `system', but returns (output, exit status)"
  (monadic-id
   (temp (make-temporary-filename))
   (p (system*/exit-code "/bin/sh" "-c"
                         (string-append command " > " temp)))
   (output (read-string-file temp))
   (trimed (string-trim-chars output "\n \t" 'both))
   (do (cons trimed p))))

(define (parse-cli args)
  (define (trim s) (string-trim-chars s "-" 'left))

  (let lp ((pos 0) (buf (list (cons #f #t))) (left args))
    (if (null? left)
        buf
        (let ((current (car left)))
          (cond
           ((string-prefix? "-" current)
            (let* ((key (trim current))
                   (cell (cons key #t))
                   (rest (cdr left)))
              (lp pos (cons cell buf) rest)))
           (#t
            (lp (1+ pos) (cons (cons pos current) buf) (cdr left))))))))

(define parse-cli-global-p (make-parameter #f))
(define parse-cli-global-default
  (let ((value #f))
    (case-lambda
      (() value)
      ((new-value)
       (set! value new-value)
       new-value))))

(define (parse-cli!)
  (let ((parsed (parse-cli (get-command-line-arguments))))
    (parse-cli-global-default parsed)
    parsed))
(define (parse-cli-parse-or-get!)
  (if (parse-cli-global-p)
      (parse-cli-global-p)
      (if (parse-cli-global-default)
          (parse-cli-global-default)
          (parse-cli!))))

(define (parse-cli-get-flag . keys)
  (let* ((parsed (parse-cli-parse-or-get!)))
    (or-map
     (lambda (key)
       (let ((ret (assoc key parsed)))
         (if (pair? ret)
             (cdr ret)
             ret)))
     keys)))

(define (parse-cli-get-switch key)
  (let ((parsed (parse-cli-parse-or-get!)))
    (let lp ((rest parsed) (prev #f))
      (if (null? rest)
          #f
          (let* ((current (car rest))
                 (k (car current))
                 (v (cdr current)))
            (if (equal? k key)
                (if prev prev
                    (throw 'command-line-flag-should-be-a-switch
                           key))
                (lp (cdr rest) v)))))))

(define (parse-cli-get-list after-key)
  (let* ((parsed (parse-cli-parse-or-get!)))
    (let lp ((rest (reverse parsed))
             (found? #f))
      (if (null? rest)
          (if found?
              (list)
              found?)
          (let ((key (car (car rest)))
                (val (cdr (car rest))))
            (if found?
                (if (integer? key)
                    (cons val (lp (cdr rest) found?))
                    (list)) ; NOTE: next is another flag/switch
                (if (equal? key after-key)
                    (lp (cdr rest) (not found?))
                    (lp (cdr rest) found?))))))))


;;;;;;;;;;;;;;;;;;
;; TREE-FUTURES ;;
;;;;;;;;;;;;;;;;;;

;; Futures, but
;; * form trees
;;   Tree-future is finished when its body and callback is evaluated
;;   and all of its children are finished
;; * multiplatform
;;   Supports different threading models
;;   Depends on abstract implementation
;;   of critical-make, thread-spawn, etc
;; * cancellable
;;   Can be intrrupted at any moment
;;   Root is cancelled first then its nodes
;;   May not be supported by threading model
;;   Only body is cancellable (and callback is guaranteed to execute *eventually*, even on cancel)
;; * stateful
;;   Provides functionality to modify
;;   future-local state.
;;   Mutations are atomic

(define-rec tree-future
  parent-index
  current-index
  children-list
  finally  ;; (: tree-future -> exit-status -> results... -> a) called after body is evaluated.  Cannot be cancelled
  callback ;; (: tree-future -> exit-status -> results... -> a) called on finish or on exception or on `cancelled?', after all children are `finished?'; it is safe to modify this structure after callback is called
  thread ;; body thread.  On non-cancel exit, callback is also called on this thread, but if this cancelled, then callback is called on a newly created thread
  evaluated?#box ;; set when body finished evaluating (maybe with error) or when cancelled. When cancelled, the value is 'cancelled
  evaluated? ;; set when body finished evaluating but by event loop, not by atomic box
  children-finished? ;; set when all children are `finished?'. Checked after `evaluated?'
  finished? ;; set when callback finished evaluating
  context ;; thunk that evaluates contexts and returns it.  #memoized
  )

(define tree-future-current (make-parameter #f))
(define tree-future-eval-context-p (make-parameter #f))
(define (tree-future-eval-context) ((tree-future-eval-context-p)))

;; Initializes tree-future env using current threading model
;; Returns an interface for this environment
(define (tree-future-get)
  (letrec
      ((message-bin null) ;; TODO: replace by atomic queue
       (message-bin-lock (dynamic-thread-critical-make))

       (init-lock (dynamic-thread-critical-make))
       (work-thread #f)

       (futures-hash (make-hash-table))
       (get-by-index
        (lambda (index)
          (hash-ref futures-hash index #f)))

       (wait-all
        (lambda ()
          (sleep-until (not work-thread))))

       (logger
        (lambda (fmt . args)
          (apply dprintln (cons fmt args))))

       (send-message
        (lambda type-args
          (with-critical
           message-bin-lock
           (set! message-bin
             (cons type-args message-bin)))))

       (run-finally
        (lambda (structure status results)
          (when (tree-future-finally structure)
            (catch-any (lambda ()
                         (apply (tree-future-finally structure)
                                (cons* structure status results)))
                       (lambda errs 0))))) ;; NOTE: errors are ignored!

       (finish
        (lambda (structure status results)

          (run-finally structure status results)
          (send-message 'finish-async structure)
          (sleep-until (tree-future-children-finished? structure))

          (let ((errs #f))
            (when (tree-future-callback structure)
              (catch-any
               (lambda ()
                 (apply (tree-future-callback structure)
                        (cons* structure status results)))
               (lambda err
                 (set! errs err))))
            (set-tree-future-finished?! structure #t)
            (send-message 'remove structure)
            (when errs
              (apply throw errs)))))

       (cancel-children
        (lambda (structure args)
          (for-each (lambda (child)
                      (cancel-future-sync child 'down (list 'parent-cancelled-with args)))
                    (tree-future-children-list structure))))

       (cancel-future-sync
        (lambda (structure mode args)
          (when (atomic-box-compare-and-set!
                 (tree-future-evaluated?#box structure)
                 #f 'cancelled) ;; NOTE: do not cancel callbacks!
            (dynamic-thread-cancel (tree-future-thread structure))
            (dynamic-thread-spawn
             (lambda ()
               (finish structure 'cancel args))))

          ;; propagate cancel not matter if target is already evaluated
          (case mode
            ((single) 0)
            ((down) (cancel-children structure args))
            ((all)
             (cancel-children structure args)
             (let ((parent (get-by-index (tree-future-parent-index structure))))
               (when parent
                 (cancel-future-sync parent 'all (list 'child-cancelled-with args))))))))

       (children-finished?
        (lambda (structure)
          (and-map tree-future-finished?
                   (tree-future-children-list structure))))

       (remove-future-sync
        (lambda (structure)
          (when (children-finished? structure)
            (when (tree-future-evaluated? structure)
              (set-tree-future-children-finished?! structure #t))
            (when (tree-future-finished? structure)
              (hash-remove! futures-hash (tree-future-current-index structure))
              (dispatch 'remove (tree-future-parent-index structure))))))

       (retry-later
        (lambda (type args)
          (apply send-message (cons type args))))

       (dispatch
        (lambda (type . args)
          (case type

            ((start)
             (match args
               (`(,structure)
                (let ((parent (get-by-index (tree-future-parent-index structure))))
                  (if (and parent
                           (tree-future-finished? parent))
                      (logger "parent is already done")
                      (begin
                        (hash-set! futures-hash
                                   (tree-future-current-index structure)
                                   structure)
                        (when parent
                          (set-tree-future-children-list!
                           parent
                           (cons structure
                                 (tree-future-children-list parent))))))))
               (else
                (logger "wrong number of arguments to 'start"))))

            ((finish-async)
             (match args
               (`(,structure)
                (set-tree-future-evaluated?! structure #t)
                (remove-future-sync structure))
               (else
                (logger "wrong number of arguments to 'finish-async"))))

            ((remove)
             (match args
               (`(,index)
                (if (tree-future? index)
                    (remove-future-sync index)
                    (let ((structure (get-by-index index)))
                      (when structure
                        (remove-future-sync structure)))))
               (else
                (logger "wrong number of arguments to 'remove"))))

            ((cancel)
             (if (or (null? args) (null? (cdr args)))
                 (logger "wrong number of arguments to 'remove")
                 (let ((index (car args))
                       (mode (cadr args))
                       (arguments (cddr args)))
                   (case mode
                     ((single down all)
                      (let* ((structure (get-by-index index)))
                        (if structure
                            (if (tree-future-thread structure)
                                (cancel-future-sync structure mode args)
                                (retry-later type args))
                            (logger "bad index"))))
                     (else
                      (logger "bad mode"))))))

            ((context) ;; also used for checking if future exists
             (match args
               (`(,target-index ,transformer)
                (let ((target (get-by-index target-index)))
                  (if target
                      (let ((current (tree-future-context target)))
                        (set-tree-future-context!
                         target
                         (let ((saved? #f)
                               (memory #f))
                           (lambda ()
                             (unless saved?
                               (set! saved? #t)
                               (set! memory (transformer (current))))
                             memory))))
                      (logger "target doesnt exist"))))
               (else
                (logger "wrong number of arguments to 'context"))))

            (else
             (logger "bad op type"))

            )))

       (recieve-loop
        (lambda ()
          (let ((sleep (dynamic-thread-get-delay-procedure)))
            (let lp ()
              (let ((val null))
                (with-critical
                 message-bin-lock
                 (begin
                   (set! val message-bin)
                   (set! message-bin null)))
                (for-each
                 (lambda (elem)
                   (apply dispatch elem))
                 (reverse val)))
              (if (hash-empty? futures-hash)
                  (set! work-thread #f)
                  (begin
                    (sleep)
                    (lp)))))))

       (maybe-start-loopin
        (lambda ()
          (init-lock
           (lambda ()
             (unless work-thread
               (set! work-thread
                 (dynamic-thread-spawn recieve-loop)))))))

       (run (lambda (target-procedure
                     finally
                     callback
                     initial-context)
              (let* ((context (lambda () initial-context))
                     (parent-index (tree-future-current))
                     (current-index (make-unique))
                     (structure (tree-future parent-index
                                             current-index
                                             null
                                             finally
                                             callback
                                             #f
                                             (make-atomic-box #f)
                                             #f #f #f
                                             context))
                     (eval-context
                      (lambda () ((tree-future-context structure)))))

                (send-message 'start structure)

                (set-tree-future-thread!
                 structure
                 (dynamic-thread-spawn
                  (lambda ()
                    (parameterize ((tree-future-current current-index)
                                   (tree-future-eval-context-p eval-context))
                      (let ((results #f)
                            (status 'undefined))
                        (catch-any
                         (lambda ()
                           (call-with-values
                               target-procedure
                             (lambda vals
                               (set! status 'ok)
                               (set! results vals))))
                         (lambda err
                           (set! status 'error)
                           (set! results err)))
                        (when (atomic-box-compare-and-set!
                               (tree-future-evaluated?#box structure)
                               #f #t)
                          (finish structure status results)))))))

                (maybe-start-loopin)
                current-index)))
       )
    (values run send-message wait-all)))

(define tree-future-send-message-p (make-parameter #f))
(define (tree-future-send-message . args)
  (apply (tree-future-send-message-p) args))

(define tree-future-run-p (make-parameter #f))
(define (tree-future-run . args)
  (apply (tree-future-run-p) args))

(define (tree-future-modify target-index transformation)
  (tree-future-send-message 'context
                            target-index
                            transformation))

(define (tree-future-cancel target-index . arguments)
  (apply tree-future-send-message
         (cons* 'cancel target-index arguments)))

(define-syntax-rule (with-new-tree-future-env . bodies)
  (let-values (((run send-message wait) (tree-future-get)))
    (parameterize ((tree-future-run-p run)
                   (tree-future-send-message-p send-message))
      (call-with-finally
       (lambda _
         (begin . bodies))
       (lambda _
         (wait))))))

;; task-oriented. Return child result, ignore callback

(define-rec tree-future-task
  touch-procedure
  child-index)

;; NOTE: if status != 'ok then throws exception
(define (tree-future-run-task-thunk thunk user-finally user-callback initial-context)
  (let ((finished? #f)
        (evaluated? #f)
        (results #f)
        (status #f)
        (structure #f))

    (define (callback cb-structure cb-status . cb-results)
      (set! structure cb-structure)
      (catch-any (lambda () (tree-future-eval-context))
                 (lambda errs 0)) ;; NOTE: ignoring errors
      (set! finished? #t)
      (when user-callback
        (apply user-callback (cons* cb-structure cb-status cb-results))))

    (define (finally structure cb-status . cb-results)
      (set! results cb-results)
      (set! status cb-status)
      (set! evaluated? #t)
      (when user-finally
        (apply user-finally (cons* structure cb-status cb-results))))

    (define (wait target)
      (case target
        ((evaluation)
         (unless evaluated?
           (sleep-until evaluated?)))
        ((children)
         (unless finished?
           (sleep-until finished?)))))

    (define (done? target)
      (case target
        ((evaluation) evaluated?)
        ((children) finished?)))

    (define touch-procedure
      (case-lambda
        (()
         (touch-procedure 'default 'children))

        ((type target)
         (case type

           ((default)
            (wait target)
            (case status
              ((ok) (apply values results))
              (else (throw 'tree-future-await-failed status results))))

           ((no-throw)
            (wait target)
            (values structure status results))

           ((check)
            (values (done? target) structure status results))

           (else
            (throw 'unknown-touch-type type))))))

    (define child-index
      (tree-future-run thunk finally callback initial-context))

    (tree-future-task
     touch-procedure
     child-index)))

(define-syntax-rule (tree-future-run-task . bodies)
  (tree-future-run-task-thunk (lambda () . bodies) #f #f #f))

(define tree-future-wait-task
  (case-lambda
    ((task)
     ((tree-future-task-touch-procedure task)))
    (tasks (map tree-future-wait-task tasks))))


;;;;;;;;;;;;;;;;;;
;; with-dynamic ;;
;;;;;;;;;;;;;;;;;;
;; & lazy-params

(define-syntax-rule (%with-dynamic-helper1 arg value . bodies)
  (if (parameter? arg)
      (parameterize ((arg value)) . bodies)
      (arg (lambda _ value) (lambda _ . bodies))))

(define-syntax with-dynamic
  (syntax-rules ()
    ((_ ((arg value)) . bodies)
     (%with-dynamic-helper1 arg value . bodies))
    ((_ ((arg value) . rest) . bodies)
     (%with-dynamic-helper1
      arg value
      (with-dynamic rest . bodies)))))

(define-syntax lazy-parameter
  (syntax-rules ()
    ((_ initial)
     (let ((p (make-parameter (memconst initial))))
       (case-lambda
        (() ((p)))
        ((value body)
         (let ((mem (memconst (value))))
           (parameterize ((p mem))
             (body)))))))
    ((_ initial converter)
     (let ((p (make-parameter (memconst initial))))
       (case-lambda
        (() ((p)))
        ((value body)
         (let ((mem (memconst (converter (value)))))
           (parameterize ((p mem))
             (body)))))))))

;;;;;;;;;;;
;; QUEUE ;;
;;;;;;;;;;;

(define make-queue
  (case-lambda
   (() (make-queue 10))
   ((initial-size)
    (let ((ret (make-vector 4)))
      (vector-set! ret 0 (make-vector initial-size))
      (vector-set! ret 1 0)
      (vector-set! ret 2 0)
      (vector-set! ret 3 'euphrates-queue)
      ret))))

(define (queue-empty? q)
  (= (vector-ref q 1) (vector-ref q 2)))

(define (queue-peek q)
  (if (= (vector-ref q 1) (vector-ref q 2))
      (throw 'empty-queue-peek)
      (vector-ref (vector-ref q 0) (vector-ref q 1))))

(define (queue-push! q value)
  (let* ((v (vector-ref q 0))
         (first (vector-ref q 1))
         (last (vector-ref q 2))
         (size (vector-length v))
         (last+1 (+ 1 last))
         (new-last (if (< last+1 size) last+1 0))
         (need-realloc? (= new-last first))
         (new-size (* 2 size))
         (v (if (not need-realloc?) v
                    (let ((ret (make-vector new-size)))
                      (let loop ((i 0) (j first))
                        (when (< i size)
                          (vector-set! ret i (vector-ref v j))
                          (loop (add1 i) (if (< (add1 j) size) (add1 j) 0))))
                      (vector-set! q 0 ret)
                      ret))))
    (if need-realloc?
        (begin
          (vector-set! q 1 0)
          (vector-set! q 2 size)
          (vector-set! v (sub1 size) value))
        (begin
          (vector-set! q 2 new-last)
          (vector-set! v last value)))))

(define queue-pop!
  (let ((private-default (make-unique)))
    (case-lambda
     ((q)
      (let ((ret (queue-pop! q private-default)))
        (if (eq? private-default ret)
            (throw 'empty-queue-peek)
            ret)))

     ((q default)
      (if (= (vector-ref q 1) (vector-ref q 2)) default
          (let* ((v (vector-ref q 0))
                 (size (vector-length v))
                 (first (vector-ref q 1))
                 (first+1 (+ 1 first))
                 (new-first (if (< first+1 size) first+1 0)))
            (vector-set! q 1 new-first)
            (vector-ref v first)))))))
