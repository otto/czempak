
%run guile

%use (say-hi say-hello) (((language "guile"))) "./kek.scm"

%var enhanced-say-hi

(define (enhanced-say-hi to-whom)
  (say-hi)
  (display ",")
  (display to-whom)
  (display "\n")
  )

;; (say-hello)

