;;;; Copyright (C) 2021, 2022  Otto Jung
;;;;
;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation; version 3 of the License.
;;;;
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(use-modules (euphrates common)
             (tiny-logic libsrc)
             (tiny-logic opsrc)
             (tiny-logic repllibsrc)
             (czempaklib helpers)
             (czempaklib register)
             (ice-9 regex)
             (ice-9 rdelim)
             (ice-9 match))

(define (read-install-structure)
  (define lns/raw
    (filter (negate string-null-or-whitespace?)
            (lines (read-string-file (get-install-table-file)))))

  (define lns/words
    (map words lns/raw))

  (define H
    (make-hash-table))

  (for-each
   (lambda (ws)
     (hash-set! H (car ws) (cdr ws)))
   lns/words)

  H)

(define (write-install-structure struct)
  (define p (open-output-file (get-install-table-file)))

  (hash-table-foreach
   struct
   (lambda (key value)
     (display key p)
     (display " " p)
     (display (unwords value) p)
     (newline p))))

(define (built-path->absolute-path path)
  (append-posix-path (get-czempak-root-prefix) path))

;;;;;;;;;;;;;;;;;;;;
;; READ REGISTERS ;;
;;;;;;;;;;;;;;;;;;;;

(define register-structure
  (read-register-structure))

(define binregister-structure
  (read-binregister-structure))

(define install-structure
  (read-install-structure))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; CHECK DELETED INSTALLS ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(for-each
 (lambda (p)
   (define path (car p))
   (unless (file-or-directory-exists? path)
     (cmd-echo-f "INFO"
                 "Installed path does not exist: ~s. Removing from registry"
                 path)
     (hash-remove! install-structure path)))
 (hash-table->alist install-structure))

;;;;;;;;;;;;;;;;;;;;;;;;;
;; MAKE INSTALLED LIST ;;
;;;;;;;;;;;;;;;;;;;;;;;;;

(define installed-alist
  (hash-table->alist install-structure))
(define installed-list ;; : list of lists of type (hash "/store/bin/"path)
  (map cdr installed-alist))

(define installed-hashes
  (map car installed-list))
(define installed-built-paths
  (map cadr installed-list))

;;;;;;;;;;;;;;;;;;;;;;;
;; REMOVAL PROCEDURE ;;
;;;;;;;;;;;;;;;;;;;;;;;

(define (gc-one H installed get-path)
  ;;;;;;;;;;;;;;;;;;;;;;;;
  ;; COLLECT NOT-NEEDED ;;
  ;;;;;;;;;;;;;;;;;;;;;;;;

  (define registered-list
    (map (compose register-entry-id cdr)
         (hash-table->alist H)))

  (define needed-packages/hash
    (take-subgraph-of H installed))

  (define not-needed-packages
    (filter
     (lambda (p)
       (not (hash-ref needed-packages/hash p #f)))
     registered-list))

  ;;;;;;;;;;;;;;;;;;;;;;;
  ;; DELETE NOT-NEEDED ;;
  ;;;;;;;;;;;;;;;;;;;;;;;

  (for-each
   (lambda (hash)
     (with-ignore-errors!
      (let ((path (get-path hash)))
        (cmd-echo "DEL" path)
        (hash-remove! H hash)
        (delete-file path))))
   not-needed-packages))

(gc-one register-structure installed-hashes package-location-from-hash)
(gc-one binregister-structure installed-built-paths built-path->absolute-path)

;;;;;;;;;;;;;;;;;;;;;;
;; UPDATE REGISTERS ;;
;;;;;;;;;;;;;;;;;;;;;;

(write-register-structure register-structure)
(write-binregister-structure binregister-structure)
(write-install-structure install-structure)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; REMOVE CACHES AND STUFF ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(execlp "rm" "rm" "-rf" (get-czempak-temp-directory))
