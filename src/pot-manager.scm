;;;; Copyright (C) 2020, 2021, 2022  Otto Vamenheis
;;;;
;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation; version 3 of the License.
;;;;
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(use-modules (euphrates common)
             (tiny-logic libsrc)
             (tiny-logic opsrc)
             (tiny-logic repllibsrc)
             (czempaklib helpers)
             (ice-9 regex)
             (ice-9 rdelim)
             (ice-9 match))

;;;;;;;;;;;;;;;;;
;; pot manager ;;
;;;;;;;;;;;;;;;;;

(define dot-pot ".czempak-pot")

(define-rec pak-config
  file ;; target file name
  identifier ;; unique identifier
  history ;; hashes, where car is the most recent one
  bresult ;; #f or the result
  updated? ;; #f initially, may be set to #t after build
  )

(define (make-pak-config filename identifier)
  (pak-config filename identifier (list) #f #f))

(define (generate-pot-id)
  (list->string (random-choice 10 alphanum#alphabet)))

(define (paks->text paks)
  (unlines
   (list-intersperse
    "" (map (compose unlines pak-config->lines) paks))))

(define (add-pak name maybe-pot)
  (let* ((identifier (generate-pot-id))
         (pot-relative (or maybe-pot dot-pot))
         (pot (if (absolute-posix-path? pot-relative) pot-relative
                  (append-posix-path (getcwd) pot-relative)))
         (existing? (file-or-directory-exists? pot))
         (do (unless existing? (create-empty-file pot)))
         (paks (read-pot-file pot))
         (pak (make-pak-config name identifier))
         (all-paks (cons pak paks)))
    (write-string-file pot (paks->text all-paks))
    (unless existing?
      (let* ((pot-id (generate-pot-id))
             (link-path (append-posix-path (get-pots-root) pot-id)))
        (write-string-file link-path pot)))))

(define (lines->pak-config lns)
  (let* ((file (list-ref lns 0))
         (identifier (list-ref lns 1))
         (history (words (list-ref-or lns 2 ""))))
    (pak-config file identifier history #f #f)))

(define (pak-config->lines config)
  (list (pak-config-file config)
        (pak-config-identifier config)
        (unwords (pak-config-history config))))

(define (build-pak-config config)
  (let* ((file (pak-config-file config))
         (bresult (with-build-inputs (list) ((final-hash hash) ?) (build file)))
         (new-hash (build-result-hash bresult))
         (maybe-last-version
          (if (null? (pak-config-history config)) #f
              (car (pak-config-history config))))) ;; NOTE: version history is reversed
    (set-pak-config-bresult! config bresult)
    (unless (equal? new-hash maybe-last-version)
      (set-pak-config-updated?! config #t)
      (set-pak-config-history!
       config (cons new-hash
                    (pak-config-history config))))))

(define (read-pot-file path)
  (let* ((pot-content (read-string-file path))
         (lns (lines pot-content))
         (partitions (list-split-on string-null-or-whitespace? lns))
         (paks (map lines->pak-config partitions)))
    paks))

(define (stage-pot maybe-target)
  (define (notify-update pak)
    (message "Updated: ~a" (pak-config-file pak)))

  (let* ((path (or maybe-target dot-pot))
         (paks (read-pot-file path))
         (do (for-each build-pak-config paks))
         (updated (filter pak-config-updated? paks))
         (return-text (paks->text paks)))
    (if (null? updated)
        (message "No updates")
        (for-each notify-update updated))
    (write-string-file path return-text)))

;; history must be non-empty
(define (pak-get-update-rewrite-struct pak)
  (let* ((hist (pak-config-history pak))
         (target (car hist))
         (old (cdr hist))
         (pairs (cartesian-map cons old (list target)))) ;; NOTE: `pairs' IS the input-struct
    pairs))

(define (get-all-paks)
  (define (read-names file)
    (let* ((paks (read-pot-file file))
           (names (map pak-config-file paks))
           (dir (dirname file))
           (fullnames (map (partial-apply1 append-posix-path dir) names)))
      fullnames))

  (let* ((pots (map car (directory-files (get-pots-root))))
         (targets (map (lambda (p) (read-string-file p)) pots))
         (files (apply append (map read-names targets))))
    files))

(define (list-installed)
  (for-each (lambda (x) (display x) (newline)) (get-all-paks)))

(define (push-update maybe-target)
  (let* ((path (or maybe-target dot-pot))
         (all-paks (read-pot-file path))
         (paks (filter (compose (negate null?) pak-config-history) all-paks))
         (rewrite-structs (map pak-get-update-rewrite-struct paks))
         (rewrite-struct (apply append rewrite-structs))
         (this-names (map pak-config-file paks))
         (this-dir (dirname (if (absolute-posix-path? maybe-target) maybe-target (append-posix-path (getcwd) path))))
         (this-fullnames (map (partial-apply1 append-posix-path this-dir) this-names))
         (this-pot-files#H (list->hash-set this-fullnames))
         (all-files (get-all-paks))
         (target-files (filter (lambda (x) (not (hash-ref this-pot-files#H x #f))) all-files)))
    (rewrite#parsed rewrite-struct target-files)))


;;;;;;;;;;
;; MAIN ;;
;;;;;;;;;;

(define (list-installed#action)
  (list-installed))

(define (push#action)
  (define maybe-target (parse-cli-get-flag 1))
  (push-update maybe-target))

(define (stage-pot#action)
  (define maybe-target (parse-cli-get-flag 1))
  (stage-pot maybe-target))

(define (add#action)
  (define name (parse-cli-get-flag 1))
  (define maybe-pot (parse-cli-get-flag 3))
  (unless name
    (errorwith "Expected target name"))
  (add-pak name maybe-pot))
