%run guile

%set (xvar 7)
%set (yvar 8)

%set (not-bound x y) (= x 3)

%set (language "guile with a star")

%use (kek-var) (language) "./kek.scm"
;; %use (kek-var) (((language "guile"))) "./kek.scm"

%for (< version 3)

%for (not-bound x 10)

(display "it was 10\n")

%end

(define haha-version "haha is less than version")

%end
%for (= 0 0)

(define haha -42)

%end

%for (xvar xxx)
(display "xvar: ")
(display (quote xxx))
(newline)
%end

%var say-bye

(define say-bye
  (lambda ()
    (display "Bye, ")
    (display haha)
    (display "!\n")
    (display "kek-var: ")
    (display kek-var)
    (display "; thekek-var: ")
    (display kek-var)
    (display " :)\n")))

