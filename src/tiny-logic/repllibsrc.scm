;;;; Copyright (C) 2020  Otto Vamenheis
;;;;
;;;; This program is free software; you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation; version 3 of the License.
;;;;
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (tiny-logic repllibsrc)
  :export (repl-loop repl-print repl-print-simple))
(use-modules (tiny-logic opsrc))
(use-modules (tiny-logic libsrc))

(define (read-sentence)
  (let loop ((buf (list)))
    (let ((word (read)))
      (cond
       ((eof-object? word)
        (cons 'eof word))
       ((equal? word '!)
        (cons 'fact (reverse buf)))
       ((equal? word '?)
        (cons 'query (reverse buf)))
       (else
        (loop (cons word buf)))))))

(define (repl-print-simple re)
  (for-each
   (lambda (xs)
     (for-each
      (lambda (x)
        (write x)
        (display " "))
      xs)
     (display "\n"))
   re))

(define repl-print
  (case-lambda
   ((handler) (repl-print handler (lambda (re) (for-each (lambda (x) (write x) (newline)) re))))
   ((handler print)
    (lambda (rules st)
      (let* ((db (create-database handler rules))
             (re (eval-query db st)))
        (cond
         ((null? re) (display "no\n"))
         ((null? (car re)) (display "yes\n"))
         (else (print re)))
        #t)))))

(define (repl-loop prompt eval rules)
  (prompt)
  (let loop ((rules rules))
    (let* ((cur (read-sentence))
           (type (car cur))
           (st (cdr cur)))
      (case type
        ((eof) rules)
        ((fact)
         (loop (cons st rules)))
        ((query)
         (if (eval rules st)
             (begin
               (prompt)
               (loop rules))
             rules))))))
