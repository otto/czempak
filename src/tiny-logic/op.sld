
(define-library (op)
  (import (scheme base))
  (export make-handler make-set make-tuple-set unify separate op+ op* ass-less divisible binary-op variable-equal?)
  (include "opsrc.scm"))
