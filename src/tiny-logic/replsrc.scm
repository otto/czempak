;;;; Copyright (C) 2020  Otto Vamenheis
;;;;
;;;; This program is free software; you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation; version 3 of the License.
;;;;
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (tiny-logic replsrc)
  :export (main))
(use-modules (tiny-logic repllibsrc))
(use-modules (tiny-logic opsrc))
(use-modules (tiny-logic libsrc))

(define handler
  (make-handler
   (= unify)
   (!= separate)
   (+ op+)
   (* op*)
   (< ass-less)
   (divisible divisible)))

(define (message msg)
  (when (current-error-port)
    (display msg (current-error-port))))
(define (prompt)
  (message "> "))
(define (go rules)
  (repl-loop prompt (repl-print handler repl-print-simple) rules))
(define (main)
  (go (list)))

(message "
  Welcome to tiny-logic
  Copyright (C) 2020  Otto Vamenheis
  This program comes with ABSOLUTELY NO WARRANTY;
  This is free software, and you are welcome to redistribute it
  under certain conditions;\n\n")

(if (or (null? (command-line)) (null? (cdr (command-line))))
    (main)
    (let ((path (cadr (command-line)))
          (rules #f))
      (message "Loading file ")
      (message path)
      (message " ...")
      (call-with-input-file path
        (lambda (file)
          (parameterize ((current-input-port file))
            (set! rules (repl-loop (lambda _ #t) (lambda _ #t) (list))))))
      (message " loaded!\n")
      (go rules)))

(message "\nBye-bye\n")
