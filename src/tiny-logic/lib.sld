
(define-library (lib)
  (import (scheme base)
          (scheme write)
          (srfi 1)
          (srfi 69)
          (srfi 9))
  (export create-database eval-query make-instantiation-check)
  (include "libsrc.scm"))
