
(use-modules (tiny-logic libsrc))
(use-modules (tiny-logic opsrc))
(define (list-sort x y) (sort y x))

(define (~a x)
  (cond
   ((string? x) x)
   ((number? x) (number->string x))
   ((symbol? x) (symbol->string x))
   ((list? x) (apply string-append (append (cons "(" (map ~a x)) (list ")"))))
   ((pair? x) (string-append "(" (~a (car x)) " . " (~a (cdr x)) ")"))
   (else "?")))

(define botom-handler
  (make-handler
   (= unify)
   (!= separate)
   (+ op+)
   (* op*)
   (< ass-less)
   (divisible divisible)
   (favorite (make-set (list 777 2 9 3)))
   (favorite2 (cons 2 (make-tuple-set '((777 2) (#t 9) (3 #f)))))))

(define-syntax test
  (syntax-rules ()
    ((_ name rules query)
     (let* ((_ (begin (display "\nTEST ") (display name) (newline)))
            (db (create-database botom-handler (quote rules)))
            (q (quote query))
            (r (eval-query db q)))
       (define check (make-instantiation-check q))
       (for-each
        (lambda (p-unordered)
          (let ((p
                 (list-sort
                  (lambda (a b)
                    (string>? (~a a) (~a b)))
                  p-unordered)))
            (display (if (check p) ">" "x"))
            (display " ")
            (write p)
            (newline)))
        r)))))

(test "SIMPLE REJECT"

 (((abc x y) (= x 3) (= x y) (= y 4))
  )

 ((abc x y))
 )

(test "SIMPLE ACCEPT"

 (((abc z k) (= 1 1))
  )

 ((abc x y))
 )

(test "NO DATABASE CASE"

 ()

 ((= 1 1))
 )

(test "COMPLEX"

 (((abc z k) (= z 8) (= p 10))
  ((yyy x) (abc x))
  ((abc x y) (= x 3) (= x y) (= y 3))
  ((abc x y) (= x "kek") (= y 5))
  ((abc z k) (= z 8) (= k 9))
  )

 ((abc x y))
 )

(test "SIMPLE SUGAR"

 (((abc 8 y) (= y 10))
  )

 ((abc x y))
 )
(test "QUERY SUGAR"

 (((abc 8 y) (= y 10))
  )

 ((abc x 10))
 )

(test "NOT EQUAL"

 (((abc 8 y) (= k 10) (= y 3) (!= y k))
  )

 ((abc x y))
 )

(test "NO ARGUMENTS"

  (((foo) (= 1 1)))

  ((foo))
  )

(test "UNIQUE"

 (((foo 1))
  ((foo 2))
  )

 ((foo x) (foo y) (!= x y))
 )

(test "SIMPLE LESS"

 ()

 ((< 2 3))
 )

(test "CHECK +"

 ()

 ((+ 2 3 5))
 )

(test "SIMPLE +"

 ()

 ((+ 2 3 z))
 )

(test "REVERSE +"

 ()

 ((+ 2 y 5))
 )

(test "REVERSE + 2"

 ()

 ((+ x 3 5))
 )

(test "SIMPLE *"

 ()

 ((* 2 3 z))
 )

(test "REVERSE *"

 ()

 ((* 2 y 6))
 )

(test "REVERSE * 2"

 ()

 ((* x 3 6))
 )

(test "REVERSE * zero"

 ()

 ((* x 0 5))
 )

(test "RANGE SIMPLE"

 ()

 ((< x 4))
 )

(test "RANGE WITH OP"

 ()

 ((< x 4) (* 2 x a))
 )

(test "RANGE DOUBLE"

 ()

 ((< y 6) (< x y) (* 2 a x))
 )

(test "SIMPLE DIVISIBLE"

 (((abc x y) (= y 10) (divisible y x))
  )

 ((abc x y))
 )

(test "SIMPLE MAKE-SET"

 ()

 ((favorite x))
 )

(test "SIMPLE MAKE-SET 2"

 ()

 ((favorite 922))
 )

(test "SIMPLE MAKE-SET 3"

 ()

 ((favorite 2))
 )

(test "BAD RULE"

 ()

 ((non-existing-rule x))
 )

(test "LIST IN DATABASE"

 (((inputs (((abc def)))))
  )

 ((inputs x))
 )

(test "TUPLE-SET 1"
 ()
 ((favorite2 x y))
 )

(test "TUPLE-SET 2"
 ()
 ((favorite2 x 2))
 )

(test "TUPLE-SET 3"
 ()
 ((favorite2 x 9))
 )

(test "TUPLE-SET 4"
 ()
 ((favorite2 777 y))
 )

(test "TUPLE-SET 5"
 ()
 ((favorite2 777 2))
 )

(test "TUPLE-SET 6"
 ()
 ((favorite2 x y z))
 )
