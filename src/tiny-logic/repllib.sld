
(define-library (repllib)
  (import (lib) (op)
          (scheme base)
          (scheme write)
          (scheme read))
  (export repl-loop repl-print repl-print-simple)
  (include "repllibsrc.scm"))
