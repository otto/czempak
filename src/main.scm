;;;; Copyright (C) 2020, 2021, 2022  Otto Vamenheis
;;;;
;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation; version 3 of the License.
;;;;
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(use-modules (euphrates common)
             (guile-interpreter)
             (tiny-logic libsrc)
             (tiny-logic opsrc)
             (tiny-logic repllibsrc)
             (czempaklib helpers)
             (ice-9 regex)
             (ice-9 rdelim)
             (ice-9 match))

(define (build#action)
  (define path (parse-cli-get-flag 1))
  (parameterize ((build-input-stream (string-append (read-all-port (current-input-port))
                                                    "(final-hash hash)?")))
    (display (build path))))

(define (package#action)
  (define path (parse-cli-get-flag 1))
  (package path))

(define (run func path query args)
  (let* ((interps (get-build-interpreters path))
         (interp-id (car interps))
         (executable (get-interpreter-executable interp-id))
         (query-s (if (string-null? query) "" (~s query)))
         (argv (apply string-append (list-intersperse " " args)))
         (argv* (cons* func query "--" path args)))
    (cmd-echo-f "INT" "~a ~a ~a -- ~s ~a" interp-id func query-s path argv)
    (cond
     ((string? executable)
      (apply execlp (cons* executable executable argv*)))
     ((procedure? executable)
      (executable argv*))
     (else
      (throw 'bad-executable-type executable)))))

(define (run-helper func path query args)
  (unless (file-or-directory-exists? path)
    (errorwith "No such file: ~s" path))
  (catch 'shell-process-failed
         (lambda _ (run func path query args) 0)
         (lambda _ 1)))

(define (run#action)
  (define head (parse-cli-get-flag 1))

  (define query-set? (string-prefix? query-cli-prefix head))
  (define query (if query-set? head ""))

  (define path (parse-cli-get-flag (if query-set? 2 1)))

  (define args
    (or (parse-cli-get-list "args")
        (parse-cli-get-list "")
        (parse-cli-get-list (if query-set? 2 1))))

  (exit (run-helper "run" path query args)))

(define (run-global#action)
  (define head (parse-cli-get-flag 1))

  (define query-set? (string-prefix? query-cli-prefix head))
  (define query (if query-set? head ""))

  (define hash (parse-cli-get-flag (if query-set? 2 1)))
  (define path (append-posix-path (get-package-root-directory) hash))

  (define args
    (or (parse-cli-get-list "args")
        (parse-cli-get-list "")
        (parse-cli-get-list (if query-set? 2 1))))

  (unless path (download hash))
  (run-helper "run" path query args))

(define (install#action)
  (define head (parse-cli-get-flag 1))

  (define query-set? (string-prefix? query-cli-prefix head))
  (define query (if query-set? head ""))

  (define path (parse-cli-get-flag (if query-set? 2 1)))
  (define install-path (parse-cli-get-flag (if query-set? 3 2)))

  (define args
    (or (parse-cli-get-list "args")
        (parse-cli-get-list "")
        (parse-cli-get-list (if query-set? 2 1))))

  (exit (run-helper "install" path query args)))

(define (list-dependencies#action)
  (define target (parse-cli-get-flag 1))
  (list-dependencies target))

(czempak-top-level-guard
 (let ((action (parse-cli-get-flag 0)))
   (match action
          ("build" (build#action))
          ("package" (package#action))
          ("run" (run#action))
          ("run-global" (run-global#action))
          ("install" (install#action))
          (other
           (errorwith "Bad action ~s" action)))))


