
(add-to-load-path ".")
(add-to-load-path "euphrates/build/src/guile")

(use-modules (euphrates common))

(dprintln "\n\n\n-----------------------------------")

(load "main.scm")

;; clear all
(let ()
  (sh "rm -rf '~a' '~a' '~a'"
      cache-filename
      (get-package-root-prefix)
      (get-import-root-prefix)))
(load "main.scm")

;; (main (list "main.scm" "init"))

;; (main (list "main.scm" "preprocess" "lol.scm" "((hello there) (xvar 2))"))

;; (main (list "main.scm" "build" "example2.scm"))

(main (list "main.scm" "run" "example2.scm" "a" "c" " d"))

;; (main (list "main.scm" "rewrite"
;;             "--input" "example.scm"
;;             "--list" "lol.scm,hahaha-replaced:kek.scm,lol.scm"))

;; (main (list "main.scm" "rewrite"
;;             "--target" "example.scm" "example2.scm"
;;             "--list" "lol.scm:kek.scm" "ekek.scm:kek.scm"))

;; (main (list "main.scm" "upgrade"
;;             "--list" "a51524f965735560af2e076b7912f2f727b46e52e14099fc120513e3b6913f2c,hahaha-replaced:ekek.scm,too"))

;; ;; rewrite
;; (let ()
;;   (define H (read-register-structure))

;;   ;; (pretty-print L)

;;   (define (print-id-rec o)
;;      (dprintln "id: ~a"
;;                (register-entry-id o))
;;      (for-each print-id-rec (register-entry-dependencies o)))

;;   (hash-for-each
;;    (lambda (key value)
;;      (print-id-rec value)
;;      (values))
;;    H)

;;   (values))

