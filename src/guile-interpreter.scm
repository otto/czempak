;;;; Copyright (C) 2020, 2021, 2022  Otto Vamenheis
;;;;
;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation; version 3 of the License.
;;;;
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (guile-interpreter))

(use-modules (czempaklib helpers))
(use-modules (hashing sha-2-guile))
(use-modules (rnrs bytevectors))
(use-modules (euphrates common))
(use-modules (tiny-logic libsrc))
(use-modules (tiny-logic opsrc))
(use-modules (tiny-logic repllibsrc))
(use-modules (ice-9 regex))
(use-modules (ice-9 rdelim))
(use-modules (ice-9 match))
(use-modules (srfi srfi-1))
(use-modules (srfi srfi-11)) ;; let-values
(use-modules (ice-9 hash-table)) ;; alist->hash-table

;;;;;;;;;;;
;; CACHE ;;
;;;;;;;;;;;

(define-rec build-struct
  hash ;; hash of the INITIAL PACKAGED file that was preprocessed to produce file with final-hash
  final-hash ;; hash of the PACKAGED file that the result was built from
  path ;; path to BUILT file, not to packaged one
  )

(define (build-struct->list bresult)
  (list (build-struct-hash bresult)
        (build-struct-final-hash bresult)
        (build-struct-path bresult)))
(define (list->build-struct lst)
  (build-struct (car lst) (cadr lst) (caddr lst)))

(define-rec dependency
  type ;; 'local or 'global
  path ;; local path or global hash
  variables ;; list of variables to import from the dependency
  inputs ;; set if this is an abstract dependency
  original ;; original dependency if this one is abstract
  )

(define cache-filename
  (append-posix-path
   (get-czempak-temp-directory)
   (string-append (get-current-link) "-guile-cache")))

(define (unserialize-top-hashtable alist)
  (define (unserialize-map o)
    (cons (car o) (cons #f (cdr o))))
  (alist->hash-table (map unserialize-map alist)))

(define (initialize-cache)
  (if (file-or-directory-exists? cache-filename)
      (let* ((p (open-input-file cache-filename))
             (alist (read p))
             (ret (unserialize-top-hashtable alist)))
        (close-port p)
        ret)
      (make-hash-table)))

(define cache-updated-once? #f)

(define (cache-update! cache serialize p value)
  (set! cache-updated-once? #t)
  (hash-set! cache p (cons serialize value)))

(define (cache-get cache deserialize p)
  (let ((x (hash-ref cache p #f)))
    (and x
         (let ((serialize (car x))
               (value (cdr x)))
           (if serialize value (deserialize value))))))

(define cache-table
  (memconst
   (initialize-cache)))

(define-syntax-rule (with-cache!#ex
                     cache0 skip override invalidate in out type args
                     . bodies)
  (if skip (begin . bodies)
      (let ((key (list type . args))
            (cache cache0))
        (if override
            (intermezzo
             result (cache-update! cache in key result)
             (begin . bodies))
            (let ((get (cache-get cache out key)))
              (or (and get (not (invalidate get)) get)
                  (intermezzo
                   result (cache-update! cache in key result)
                   (begin . bodies))))))))

(define %define#cached-global-counter 0)

(define-syntax-rule (cached-lambda args
                                   bindings
                                   (cache0 skip override invalidate in out get-key)
                                   . bodies)
  (let ((cache cache0)
        (type %define#cached-global-counter))
    (set! %define#cached-global-counter (+ 1 %define#cached-global-counter))
    (lambda args
      (let* bindings
        (with-cache!#ex
         cache
         skip
         override
         invalidate
         in out
         type
         get-key
         (begin . bodies))))))

(define-syntax-rule (define#cached (name . args) . argv)
  (define name (cached-lambda args . argv)))

(define (check-cache-mtime mtime)
  (lambda (p)
    (define saved-mtime (car p))
    (and saved-mtime
         (not (equal? mtime saved-mtime)))))

(define-syntax-rule (define#file-cached (name file . rest) (in out additional-keys) body)
  (define name
    (let ((fn (cached-lambda
               (file . rest) ;; args
               ((srcfile (if (string? file) (make-sourcefile file) file))
                (path (sourcefile-path srcfile))
                (mtime (and (local-file? path) (get-mtime path)))) ;; bindings

               ((cache-table) ;; cache
                (temporary-file? path) ;; skip
                #f ;; override
                (check-cache-mtime mtime) ;; invalidate

                (lambda (p) (cons (car p) (in (cdr p)))) ;; in
                (lambda (p) (cons (car p) (out (cdr p)))) ;; out

                (path . additional-keys)) ;; keys

               (cons
                mtime
                (body srcfile path mtime)))))
      (lambda args
        (cdr (apply fn args))))))

(define-syntax-rule (define#hash-cached (name file . rest) (in out additional-keys) body)
  (define name
    (cached-lambda
     (file . rest) ;; args

     ((srcfile (cond ((string? file)
                      (make-sourcefile file))
                     ((iprog-fileinfo? file)
                      (iprog-fileinfo-srcfile file))
                     (else file)))
      (path (sourcefile-path srcfile))
      (hash (sourcefile-hash srcfile))) ;; bindings

     ((cache-table) ;; cache
      (temporary-file? path) ;; skip
      #f ;; override
      (const #f) ;; invalidate

      in out

      (hash . additional-keys)) ;; keys

     (body srcfile))))

(define %local-mtime-table (make-hash-table))
(define (get-mtime path)
  (or (hash-ref %local-mtime-table path #f)
      (letin
       (srcfile (make-iprog-fileinfo (make-sourcefile path)))
       (deps (iprog-fileinfo-dependencies srcfile))
       (local-deps (filter dependency-local? deps))
       (dep-paths (map dependency-path local-deps))
       (do (intermezzo
            result (hash-set! %local-mtime-table path result)
            (apply max
                   (cons (file-mtime path)
                         (map get-mtime dep-paths)))))))) ;; TODO: [local loop]

(define-syntax-rule (with-double-cache! cache skip in out key1 args key2 invalidate1 body1 body2)
  (if skip body2
      (with-cache!#ex
       (with-cache!#ex
        cache ;; cache
        #f ;; skip
        #f ;; override
        invalidate1 ;; invalidate
        serialize-top-hashtable ;; in
        unserialize-top-hashtable ;; out
        key1 args
        body1) ;; cache
       #f ;; skip
       #f ;; override
       (const #f) ;; invalidate
       in out
       key2 ()
       body2)))

(define (serialize-top-hashtable h)
  (hash-map->list
   (lambda (key value)
     (cons key
           (let ((serialize (car value))
                 (x (cdr value)))
             (if serialize (serialize x) x))))
   h))

(define (cache-save-to-file!)
  (when cache-updated-once?
    (let* ((p (open-output-file cache-filename))
           (alist (serialize-top-hashtable (cache-table))))
      ;; (parameterize ((current-output-port p))
      ;;   (pretty-print alist))
      (write alist p)
      (close-port p))))

(define (merge-hash-tables base additional)
  (define (func key value)
    (hash-set! base key value))
  (hash-for-each func additional))

(define (merge-single! v)
  (let* ((key (car v))
         (cache (cache-table))
         (get (cache-get cache identity key)))
    (if (and get (hash-table? get))
        (let ((other (unserialize-top-hashtable (cdr v))))
          (set! cache-updated-once? #t)
          (merge-hash-tables get other))
        (cache-update! cache #f key (cdr v)))))

(define (merge-cache!)
  (let* ((p (open-input-file cache-filename))
         (alist (read p)))
    (for-each merge-single! alist)
    (close-port p)))

(define-syntax-rule (with-cache-sync . bodies)
  (begin
    (cache-save-to-file!)
    (intermezzo
     result (merge-cache!)
     (begin . bodies))))

;;;;;;;;;;;;;
;; HELPERS ;;
;;;;;;;;;;;;;

(define (sha256 str)
  (sha-256->string (sha-256 (string->utf8 str))))
(define (sha256-file path)
  (sha256 (czempak-read-file path)))

(define#file-cached (get-file-hash#ex file maybe-forced)
  (identity identity ()) ;; in/out

  (lambda (srcfile path mtime)
    (or maybe-forced
        (intermezzo
         result (cmd-echo "HASH" path result)
         (sha256-file path)))))

(define (get-file-hash file)
  (get-file-hash#ex file #f))

;;;;;;;;;;;;;;;;;;
;; PREPROCESSOR ;;
;;;;;;;;;;;;;;;;;;

(define-rec preprocessor-line
  command ;; command like 'set, 'for, 'end, 'use, 'run
  args ;; arguments as string
  )

(define preprocessor-prefix interpreter-run-keyword-prefix)

(define (pline->string pline)
  (string-append preprocessor-prefix
                 (~a (preprocessor-line-command pline))
                 (preprocessor-line-args pline)))

(define preprocess-map-single
  (let ((prefix-len (string-length preprocessor-prefix)))
    (lambda (line)
      (if (string-prefix? preprocessor-prefix line)
          (letin
           (ws (substring line prefix-len))
           (i (or (string-index ws #\space)
                  (string-length ws)))
           (f (substring ws 0 i))
           (r (substring ws i))
           (cmd (string->symbol f))
           (do (preprocessor-line cmd r)))
          line))))

(define (preprocess-map-lines lns)
  (map preprocess-map-single lns))

(define-rec preprocessor-for-group
  cline ;; : preprocessor-line
  lines ;; : list of sub lines
  )

(define (pline-of-type? cmd line)
  (and (preprocessor-line? line)
       (eq? cmd (preprocessor-line-command line))))

(define-syntax-rule (define-preprocessor-word key name sym-name check-name)
  (define-values (sym-name name check-name)
    (values (quote key)
            (symbol->string (quote key))
            (lambda (line) (pline-of-type? (quote key) line)))))

(define-preprocessor-word use preprocessor-use-cmd preprocessor-use-cmd-sym pline-is-use?)
(define-preprocessor-word var preprocessor-var-cmd preprocessor-var-cmd-sym pline-is-var?)
(define-preprocessor-word for preprocessor-for-cmd preprocessor-for-cmd-sym pline-is-for?)
(define-preprocessor-word end preprocessor-end-cmd preprocessor-end-cmd-sym pline-is-end?)
(define-preprocessor-word set preprocessor-set-cmd preprocessor-set-cmd-sym pline-is-set?)
(define-preprocessor-word run preprocessor-run-cmd preprocessor-run-cmd-sym pline-is-run?)

(define (preprocessor-get-lines content)
  (define lns
    (lines content))
  (define maped
    (preprocess-map-lines lns))
  (map (curry-if pline-is-use? use-pline->dependency) maped))

(define get-guile-version
  (memconst
   (let* ((str (with-no-shell-log (sh-re "guile -v | grep -o -E -e '[0-9]+(\\.[0-9]+)+'")))
          (sp (string-split#simple str #\.)))
     (append (map string->number sp)
             (map (const 0) (range (- 3 (length sp))))))))

(define preprocessor-handler
  (make-handler
   (= unify)
   (!= separate)
   (+ op+)
   (* op*)
   (< ass-less)
   (divisible divisible)
   (COMPILER (make-set (list "guile")))
   (COMPILER-VERSION (cons 3 (make-tuple-set (list (get-guile-version)))))))

(define (preprocess-plines preset-stmts maped)
  (define (reverse-for-group! g)
    (set-preprocessor-for-group-lines!
     g (reverse (preprocessor-for-group-lines g))))

  (define (group-fors lines-initial)
    (let lp ((lines lines-initial)
             (indent 0)
             (cur #f)
             (ret (list)))
      (if (null? lines)
          (if cur (errorwith "Unclosed ~a~a directive" preprocessor-prefix preprocessor-for-cmd)
              (reverse ret))
          (let* ((line (car lines))
                 (for? (pline-is-for? line))
                 (end? (pline-is-end? line))
                 (new-indent
                  (+ indent
                     (cond (for? 1) (end? -1) (else 0)))))

            (when (< new-indent 0)
              (errorwith
               "~a~a directive without a ~a~a directive at ~s"
               preprocessor-prefix preprocessor-end-cmd preprocessor-prefix preprocessor-for-cmd
               (- (length lines-initial)
                  (length lines))))

            (cond
             ((and for? (= indent 0) (not cur))
              (lp (cdr lines)
                  new-indent
                  (preprocessor-for-group line (list))
                  ret))

             ((and end? (= indent 1) cur)
              (reverse-for-group! cur)
              (lp (cdr lines)
                  new-indent
                  #f
                  (cons cur ret)))

             (cur
              (set-preprocessor-for-group-lines!
               cur
               (cons line (preprocessor-for-group-lines cur)))

              (lp (cdr lines)
                  new-indent
                  cur
                  ret))

             (else
              (lp (cdr lines)
                  new-indent
                  cur
                  (cons line ret))))))))

  (define (modify-use-pline dep sets)
    ;; TODO: also add things that this dep depends on
    (define (swap x)
      (define (is-it? y)
        (equal? x (car (car y))))
      (filter is-it? sets))
    (let* ((inputs0 (or (dependency-inputs dep) (list)))
           (inputs (apply append (map (curry-if (negate pair?) swap list) inputs0))))
      (unless (equal? inputs0 inputs)
        (set-dependency-inputs! dep inputs))
      dep))

  (define (handle-lines current-sets lns)
    (define grouped (group-fors lns))

    (define (for-group->lines sets g)
      (letin
       (db (create-database preprocessor-handler sets))
       (cline (preprocessor-for-group-cline g))
       (args (preprocessor-line-args cline))
       (query (preprocessor-line->syntax cline))
       (check (make-instantiation-check query))
       (alist-multiple (eval-query db query))
       (lns (preprocessor-for-group-lines g))
       (repl
        (lambda (alist)
          (if (check alist)
              (replace-words-in-lines alist lns)
              (errorwith "Not every variable instantiated in ~s" query))))
       (new-lines-buk (map repl alist-multiple))
       (new-lines (apply append new-lines-buk))))

    (let lp ((buf grouped) (sets current-sets))
      (if (null? buf) (list)
          (let ((cur (car buf)))
            (cond
             ((pline-is-set? cur)
              (lp (cdr buf)
                  (append sets (list (preprocessor-line->syntax cur)))))
             ((dependency? cur)
              (cons (modify-use-pline cur sets) (lp (cdr buf) sets)))
             ((preprocessor-for-group? cur)
              (let* ((new-lines (for-group->lines sets cur))
                     (handled (handle-lines sets new-lines)))
                (append handled
                        (lp (cdr buf) sets))))
             (else
              (cons cur (lp (cdr buf) sets))))))))

  (define (preprocessor-line->syntax pline)
    (let* ((body-s (preprocessor-line-args pline)))
      (read-list body-s)))
  (define (sets->logic-db-syntax sets)
    (map preprocessor-line->syntax sets))

  (define handled
    (handle-lines preset-stmts maped))

  handled)

(define (preprocess-text only-strings? preset-stmts content)
  (define plines
    (preprocessor-get-lines content))
  (define handled
    (preprocess-plines preset-stmts plines))
  (if only-strings?
      (plines->clear-text handled)
      (plines->text handled)))

(define (preprocess-file only-strings? preset-stmts filepath)
  (let ((content (czempak-read-file filepath)))
    (preprocess-text only-strings? preset-stmts content)))

(define (preprocess args-s target)
  (define args
    (with-input-from-string args-s
      (lambda () (read))))

  (define-values (ret deps)
    (preprocess-file #f args target))

  (display ret))

(define (plines->text plines)
  (define (pline->line pline)
    (cond
     ((string? pline) pline)
     ((dependency? pline) (dependency->string pline))
     ((preprocessor-line? pline) (pline->string pline))
     (else (throw 'unexpected-type-in-plines->text))))
  (unlines (map pline->line plines)))

(define (plines->clear-text plines)
  (unlines (filter string? plines)))

(define (dependency->list dep)
  (and dep
       (list (dependency-type dep)
             (dependency-path dep)
             (dependency-variables dep)
             (dependency-inputs dep)
             (dependency->list (dependency-original dep)))))
(define (list->dependency lst)
  (and lst
       (dependency (list-ref lst 0) ;; type
                   (list-ref lst 1) ;; path
                   (list-ref lst 2) ;; variables
                   (list-ref lst 3) ;; inputs
                   (list->dependency (list-ref lst 4)))))

(define (dependency->use-pline dep)
  (define args (dependency-inputs dep))
  (define type-path
    (case (dependency-type dep)
      ((global) (string-append
                 " \""
                 (dependency-path dep)
                 "\""))
      ((local) (dependency-path dep))
      (else (string-append
             " ("
             (~a (dependency-type dep))
             " \""
             (~a (dependency-path dep))
             "\")"))))

  (preprocessor-line
   preprocessor-use-cmd-sym
   (string-append " " (~a (map symbol->string (dependency-variables dep)))
                  (or (and args (string-append " " (~s args))) "")
                  type-path)))

(define (dependency->string dep)
  (string-append
   preprocessor-prefix
   preprocessor-use-cmd
   (preprocessor-line-args (dependency->use-pline dep))))

(define (use-pline->dependency use-preprocessor-line)
  (define args-s (preprocessor-line-args use-preprocessor-line))
  (define args (read-list args-s))

  (define len (length args))

  (define-values
      (type-path-index args-index)
    (case len
      ((2) (values 1 -1))
      ((3) (values 2 1))
      (else (errorwith "Expected 2 or 3 arguments in ~s" args-s))))

  (define names
    (or (list-ref-or args 0)
        (errorwith "Dependency does not have names in ~s" args-s)))

  (define type-path
    (or (list-ref-or args type-path-index)
        (errorwith "Dependency does not have a path in ~s" args-s)))

  (define _
    (unless (or (string? type-path)
                (and (list? type-path) (= 2 (length type-path))))
      (errorwith "First argument of ~a-pline is not list in ~s" preprocessor-use-cmd args-s)))

  (define type
    (if (string? type-path)
        (if (string-prefix? "./" type-path)
            'local
            'global)
        (car type-path)))

  (define path
    (if (string? type-path) type-path
        (cadr type-path)))

  (define arguments
    (list-ref-or args args-index))

  (define original #f)

  (dependency type path names arguments original))

(define (dependency-local? dep)
  (eq? 'local (dependency-type dep)))

(define (dependency-global? dep)
  (eq? 'global (dependency-type dep)))

(define (dependency-abstract? dep)
  (and (dependency-inputs dep) (not (null? (dependency-inputs dep)))))

(define (dependency-concrete? dep)
  (not (dependency-abstract? dep)))

;; discards inputs!
;; saves variables
(define (concrete-global-dependency hash previous)
  (dependency 'global ;; type
              hash ;; path
              (dependency-variables previous) ;; variables
              #f ;; input
              previous ;; original
              ))

;; saves inputs
;; saves variables
(define (any-global-dependency hash previous)
  (dependency 'global ;; type
              hash ;; path
              (dependency-variables previous) ;; variables
              (dependency-inputs previous) ;; input
              previous ;; original
              ))

(define (dependency-replace-path dep path-type path)
  (dependency path-type
              path
              (dependency-variables dep)
              (dependency-inputs dep)
              dep))

(define (get-initial-dependency dep)
  (let lp ((d dep))
    (if (dependency-original d)
        (lp (dependency-original d))
        d)))

(define (hashes-of-packaged-dependencies pdeps)
  (define (f dep)
    (unless (dependency-global? dep)
      (throw 'packaged-dependency-has-to-be-global dep))
    (dependency-path dep))
  (map f pdeps))

(define (get-original-dependencies dependencies)
  (define deps (or dependencies (list)))

  (define (check d)
    (and (dependency-original d)
         (dependency-global? (dependency-original d))))

  (define originals
    (map (curry-if check dependency-original) deps))

  (hashes-of-packaged-dependencies originals))

;;;;;;;;;;;;;;;;
;; SOURCEFILE ;;
;;;;;;;;;;;;;;;;

(define sourcefile-hash
  (cached-record-accessor
   sourcefile-%hash
   (compose get-file-hash sourcefile-path)
   set-sourcefile-%hash!))

(define sourcefile-content
  (cached-record-accessor
   sourcefile-%content
   (compose czempak-read-file sourcefile-path)
   set-sourcefile-%content!))

(define (sourcefile-update! file)
  (set-sourcefile-%content! file #f)
  (set-sourcefile-%hash! file #f))

(define (sourcefile-copy!#logical srcfile destination)
  (define path (sourcefile-path srcfile))

  ;; TODO: investigate why this happens
  (unless (equal? path destination)
    (set-sourcefile-path! srcfile destination)
    (hash-set! %sourcefile-cache destination srcfile)

    (when (sourcefile-%hash srcfile)
      ;; assert that new file has the same hash, this is very useful.
      (get-file-hash#ex destination (sourcefile-%hash srcfile)))))

(define (sourcefile-move!#logical srcfile destination)
  (hash-set! %sourcefile-cache (sourcefile-path srcfile) #f)
  (sourcefile-copy!#logical srcfile destination))

;;;;;;;;;;;;;
;; GENERAL ;;
;;;;;;;;;;;;;

(define (dependency->location dep)
  (cond
   ((dependency-local? dep)
    (dependency-path dep))
   ((dependency-global? dep)
    (package-location-from-hash (dependency-path dep)))
   (else
    (throw 'expected-local-or-global-dependency dep))))

(define (project-file command source-path destination-path)
  (case command
    ((copy) (copy-source source-path destination-path))
    ((move) (move-source source-path destination-path))
    (else (throw 'bad-command command))))

(define (save-code destination-path command source-path)
  (values destination-path
          (intermezzo
           result (when result (project-file command source-path destination-path))
           (not (file-or-directory-exists? destination-path)))))

(define (save-built-code command hash source-path)
  (let ((destination-path (import-location-from-hash hash)))
    (save-code destination-path command source-path)))

(define (save-packaged-code command hash source-path)
  (let ((destination-path (package-location-from-hash hash)))
    (save-code destination-path command source-path)))

;;;;;;;;;;;;;;;;;;;;;;;
;; iprog-interpreter ;;
;;;;;;;;;;;;;;;;;;;;;;;
; class of interpreters for interpreted languages such as guile or text

(define-rec iprog-fileinfo
  srcfile ;; sourcefile? ;; NOTE: need to keep in sync with this
  %plines ;; : list-of string? | #f
  %dependencies ;; list-of dependency? | #f
  )

(define %call#memoized-global-table
  (make-hash-table))

;; NOTE: does not memoize arguments!
;; NOTE: works only on functions with single return value
(define-syntax-rule (call#memoized function0 . args0)
  (let* ((function function0)
         (args (list . args0))
         (key (cons function args))
         (get (hash-get-handle %call#memoized-global-table key)))
    (if get (cdr get)
        (intermezzo
         result (hash-set! %call#memoized-global-table key result)
         (apply function args)))))

;; NOTE: works only on single values
(define-syntax-rule (define#memoized (name . args) . bodies)
  (define name
    (let* ((memory (make-hash-table))
           (bad (make-unique)))
      (lambda args
        (let ((get (hash-ref memory (list . args) bad)))
          (if (eq? get bad)
              (intermezzo
               result (hash-set! memory (list . args) result)
               (let () . bodies))
              get))))))

(define (localize-path dir path)
  (if (string-prefix? "/" path) path
      (simplify-posix-path (append-posix-path dir path))))

;; accepts a non-empty list and returns a list of succesive applications of function
(define (list-scan function lst)
  (let loop ((lst (cdr lst)) (acc (car lst)) (buf (list (car lst))))
    (if (null? lst) (reverse buf)
        (let ((x (function acc (car lst))))
          (loop (cdr lst) x (cons x buf))))))

(define#memoized (is-file-a-symlink? path)
  (and (not (equal? "/" path))
       (case (stat:type (stat path)) ((symlink) #t) (else #f))))

(define (get-real-file-location path)
  (define link? (is-file-a-symlink? path))
  (if link? (readlink path) path))

(define#memoized (get-real-file-directory path)
  (dirname (get-real-file-location path)))

(define (localize-pline! parent-path)
  (let* ((dir (get-real-file-directory parent-path)))
    (lambda (pline)
      (if (and (dependency? pline)
               (dependency-local? pline))
          (let* ((path (dependency-path pline))
                 (loc (localize-path dir path)))
            (set-dependency-path! pline loc))))))

(define iprog-fileinfo-plines
  (cached-record-accessor
   iprog-fileinfo-%plines
   (lambda (info)
     (letin
      (srcfile (iprog-fileinfo-srcfile info))
      (content (sourcefile-content srcfile))
      (plines (preprocessor-get-lines content))
      (path (sourcefile-path srcfile))
      (do (for-each (localize-pline! path) plines))
      (do plines)))
   set-iprog-fileinfo-%plines!))

(define iprog-fileinfo-dependencies
  (cached-record-accessor
   iprog-fileinfo-%dependencies
   (comp iprog-fileinfo-plines
         (filter dependency?))
   set-iprog-fileinfo-%dependencies!))

(define %iprog-fileinfo-cache
  (make-hash-table))

(define (make-iprog-fileinfo srcfile)
  (define path (sourcefile-path srcfile))
  (or (hash-ref %iprog-fileinfo-cache path #f)
      (intermezzo
       result (hash-set! %iprog-fileinfo-cache path result)
       (iprog-fileinfo srcfile #f #f))))

(define (guile-remote-package interp-id)
  (lambda (interps srcfile)
    (with-cache-sync
     ((interpreter-package interp-id) interps srcfile))))

(define (guile-local-package interps srcfile)
  (guile-package srcfile))

(define guile-abstract-package
  (interpreter-do
   (lambda (interp-id)
     (if (equal? interp-id guile-name)
         guile-local-package
         (guile-remote-package interp-id)))))

(define (guile-remote-build interp-id)
  (lambda (interps srcfile)
    (with-cache-sync
     ((interpreter-build interp-id) interps srcfile))))

(define (guile-local-build interps srcfile)
  (with-output-to-string
    (lambda _
      (with-input-from-string (build-input-stream)
        (lambda _ (iprog-interp-build srcfile))))))

(define guile-abstract-build
  (interpreter-do
   (lambda (interp-id)
     (if (equal? interp-id guile-name)
         guile-local-build
         (guile-remote-build interp-id)))))

(define (build-dependency dep)
  (define _1 (assert (dependency-global? dep)))
  (define original-hash (dependency-path dep))
  (define package-srcfile (make-sourcefile-from-hash original-hash))
  (define path (sourcefile-path package-srcfile))

  (unless (file-or-directory-exists? path)
    (download original-hash))

  (let* ((inputs (or (dependency-inputs dep) (list)))
         (bresult (with-build-inputs inputs ((final-hash final-hash) ?) (guile-abstract-build path)))
         (hash (build-result-final-hash bresult)))
    (if (equal? hash original-hash) #f
        (begin
          (cmd-echo-f "BUILT" "~a ~a" path hash)
          (concrete-global-dependency hash dep)))))

;; NOTE: assumes that all use-plines are global!
(define (rewrite-plines plines resolved-deps)
  (define H (make-hash-table))

  (define (add-to-hashtable dep)
    (unless (dependency-original dep)
      (throw 'cannot-rewrite-dependency-because-forgot-original dep))
    (let lp ((d (dependency-original dep)))
      (when d
        (let ((hash (dependency-path d)))
          (hash-set! H hash dep)
          (lp (dependency-original d))))))
  (define _
    (for-each add-to-hashtable resolved-deps))

  (define (rewrite-one d)
    (let* ((hash (dependency-path d))
           (target-pline (hash-ref H hash d)))
      target-pline))

  (map (curry-if dependency? rewrite-one) plines))

(define (guile-globalize-dependency dep)
  (if (dependency-global? dep) #f
      (let* ((path (dependency-path dep))
             (hash (guile-abstract-package path)))
        (any-global-dependency hash dep))))

(define (rewrite-fileinfo-dependencies info new-plines0 new-deps save? drop-run?)
  (letin
   (srcfile (iprog-fileinfo-srcfile info))
   (path (sourcefile-path srcfile))
   (new-plines (if drop-run? (filter (negate pline-is-run?) new-plines0) new-plines0))
   (as-text (plines->text new-plines))
   (output-path (write-to-temporary path as-text))
   (out-src (make-sourcefile output-path))
   (out-info (iprog-fileinfo out-src new-plines new-deps))
   (do (when save? (save-package out-info)))
   (do out-info)))

(define (abstract-handle-pak dep-transformation save? drop-run?)
  (lambda (info)
    (define srcfile (iprog-fileinfo-srcfile info))
    (define dependencies (iprog-fileinfo-dependencies info))
    (define plines (iprog-fileinfo-plines info))
    (define path (sourcefile-path srcfile))
    (define hash (sourcefile-hash srcfile))

    (let* ((transformed (map (lambda (d) (cons (dep-transformation d) d)) dependencies))
           (resolved (filter identity (map car transformed))))
      (if (null? resolved)
          (begin
            (when save? (save-package info))
            (if drop-run? (remove-run-line info)
                info))
          (let* ((new-plines (rewrite-plines plines resolved)) ;; TODO: simplify rewrite having `transformed' maping
                 (new-deps (map (curry-if car car cdr) transformed)))
            (rewrite-fileinfo-dependencies info new-plines new-deps save? drop-run?))))))

(define globalize-pak
  (abstract-handle-pak guile-globalize-dependency #t #f))

;; TODO: cache this
(define (preprocess-pak info)
  (define srcfile (iprog-fileinfo-srcfile info))
  (define path (sourcefile-path srcfile))
  (define plines (iprog-fileinfo-plines info))
  (define dependencies (iprog-fileinfo-dependencies info))
  ;; ASSUME: only %for changes output
  (if (not (or-map pline-is-for? plines)) info
      (letin
       (do (cmd-echo "CPP" path))
       (preprocessor-inputs (append (current-inputs) (current-environment)))
       (new-plines (preprocess-plines preprocessor-inputs plines))
       (as-text (plines->text new-plines))
       (new-deps (filter dependency? new-plines))
       (output-path (write-to-temporary path as-text))
       (new-src (make-sourcefile output-path))
       (do (iprog-fileinfo new-src new-plines new-deps)))))

(define build-deps
  (abstract-handle-pak build-dependency #f #t))

;; This is very important!
;; It is needed to express that same sourcefiles with different run lines are equal
(define (remove-run-line info)
  (letin
   (srcfile (iprog-fileinfo-srcfile info))
   (path (sourcefile-path srcfile))
   (plines (iprog-fileinfo-plines info))
   (no-run (filter (negate pline-is-run?) plines))
   (text (plines->text no-run))
   (built-path (write-to-temporary path text))
   (new-src (make-sourcefile built-path))
   (new-info (iprog-fileinfo new-src no-run (iprog-fileinfo-%dependencies info)))))

(define (build-pak initial-hash info)
  (letin
   (packaged-file (iprog-fileinfo-srcfile info))
   (packaged-path (sourcefile-path packaged-file))
   (do (cmd-echo "BUILD" packaged-path))
   (plines (iprog-fileinfo-plines info))
   (dependencies (iprog-fileinfo-dependencies info))
   (final-hash (sourcefile-hash packaged-file))
   (vars (filter pline-is-var? plines))
   (header (get-header final-hash dependencies vars))
   (rest (plines->clear-text plines))
   (built-path (write-to-temporary packaged-path header rest))
   ((build-dest rebuild?) (save-build dependencies final-hash built-path)) ;; NOTE: hash of PACKAGED-FILE
   (do (build-struct initial-hash final-hash build-dest))))

(define#hash-cached (handle-pak info)
  (build-struct->list ;; in
   list->build-struct ;; out
   ((current-environment)
    (current-inputs))) ;; additional-keys

  (lambda _
    (define initial-hash (sourcefile-hash (iprog-fileinfo-srcfile info)))
    (appcomp info
             preprocess-pak
             build-deps
             (build-pak initial-hash))))

(define (save-build dependencies final-hash path)
  (define command (if (temporary-file? path) 'move 'copy))

  (define-values (packaged-dest built?)
    (save-built-code command final-hash path))

  (when built?
    (register-bin (import-location-from-hash final-hash)
                  (map (compose import-location-from-hash
                                dependency-path)
                       dependencies)))

  (values packaged-dest built?))

;; returns modified info
(define (save-package info)
  (define srcfile (iprog-fileinfo-srcfile info))
  (define hash (sourcefile-hash srcfile))
  (define path (sourcefile-path srcfile))
  (define command (if (temporary-file? path) 'move 'copy))

  (let-values
      (((packaged-dest packaged?)
        (save-packaged-code command hash path)))
    (if packaged?
        (let ((dependencies (iprog-fileinfo-dependencies info)))
          (cmd-echo "SAVE" path (package-location-from-hash hash))
          (register-package hash (get-original-dependencies dependencies))
          (case command
            ((copy) (sourcefile-copy!#logical srcfile packaged-dest))
            ((move) (sourcefile-move!#logical srcfile packaged-dest))
            (else (throw 'bad-command command))))
        (sourcefile-copy!#logical srcfile packaged-dest)))

  info)

(define#file-cached (iprog-interp-package#ex file)
  ((compose sourcefile-path iprog-fileinfo-srcfile) ;; in
   (compose make-iprog-fileinfo make-sourcefile) ;; out
   ()) ;; additional-keys

  (lambda (srcfile path mtime)
    (appcomp srcfile
             make-iprog-fileinfo
             globalize-pak
             save-package)))

(define (iprog-interp-build universe-file)
  (define srcfile
    (if (string? universe-file) (make-sourcefile universe-file)
        universe-file))

  (define (full-build)
    (handle-pak (iprog-interp-package#ex srcfile)))

  (define current-rules (make-parameter (list)))

  (define program-accepts
    (lambda (args ctx)
      (catch-any
       (lambda _
         (define input-stream (~s (append (current-environment) (current-inputs))))
         (guile-run #f input-stream universe-file (map ~a args))
         #t)
       (lambda errors
         ;; TODO: return as a query result
         (apply cmd-echo (cons "ERROR" (map ~a (apply append (map (curry-if (negate pair?) list) errors)))))
         #f))))

  (define handler
    (compose-under
     or
     preprocessor-handler
     (make-handler
      (= unify)
      (build-errors (make-set (catch-any (lambda _ (full-build) (list)) (lambda ex ex))))
      (build-dest (make-set (list (get-build-dest))))
      (final-hash (make-set (list (get-build-final-hash))))
      (hash (make-set (list (get-build-hash))))
      (package-dest (make-set (list (guile-package universe-file))))
      (program-accepts program-accepts)
      (inlined-dest (make-set (list (get-inlined))))
      (dependencies (make-set (iprog-interp-get-dependencies srcfile))))))

  (define (get-list-of name)
    (lambda _
      (let* ((db (create-database handler (current-rules)))
             (key (gensym))
             (query (list (list name key)))
             (re (eval-query db query)))
        ((query-result-find-multiple key) re))))

  (define get-inputs (get-list-of query-inputs-name))
  (define get-environment (get-list-of query-environment-name))

  (define (get-inlined)
    (parameterize ((current-inputs (get-inputs))
                   (current-environment (get-environment)))
      (inline (get-build-dest))))

  (define (get-build-dest)
    (parameterize ((current-inputs (get-inputs))
                   (current-environment (get-environment)))
      (build-struct-path (full-build))))
  (define (get-build-hash)
    (parameterize ((current-inputs (get-inputs))
                   (current-environment (get-environment)))
      (build-struct-hash (full-build))))
  (define (get-build-final-hash)
    (parameterize ((current-inputs (get-inputs))
                   (current-environment (get-environment)))
      (build-struct-final-hash (full-build))))

  (define (print re)
    (for-each
     (lambda (x) (write x) (newline)) re))
  (define (evaluator rules st)
    (parameterize ((current-rules rules))
      (let* ((db (create-database handler rules))
             (re (eval-query db st)))
        (cond
         ((null? re) (display "no\n"))
         ((null? (car re)) (display "yes\n"))
         (else (print re))))))

  (repl-loop (lambda _ #t) evaluator (list)))

(define iprog-interp-package
  (compose iprog-fileinfo-srcfile iprog-interp-package#ex))

(define (iprog-interp-get-dependencies target)
  (define (show d)
    (let ((path (dependency-path d)))
      (if (dependency-local? d)
          (if (string-prefix? "./" path) path (string-append "./" path))
          path)))
  (letin
   (srcfile
    (if (string? target) (make-sourcefile target) target))
   (deps
    (iprog-fileinfo-dependencies (make-iprog-fileinfo srcfile)))
   (initial-deps
    (map get-initial-dependency deps))
   (global-hashes
    (map show initial-deps))))

(define current-interpreter (make-parameter #f)) ;; FIXME: useless?
(define current-inputs (make-parameter #f))

;;;;;;;;;;;;;
;; INLINER ;;
;;;;;;;;;;;;;

(define (inline#pure built-path)
  (define (get-iname len)
    (lambda (p)
      (letin
       (index (car p))
       (value (cdr p))
       (return (and (> index 1) ;; skip "define-module" and its name
                    (< index (- len 2)) ;; skip ":export"
                    (= 1 (remainder index 2)) ;; skip ":use-module"
                    (symbol->string (car (car value))))))))

  (define ret0
    (let loop ((built-path built-path))
      (letin
       (port (open-input-file built-path))
       (header (read port))
       (rest (read-all-port port))
       (do (close-port port))
       (exports0 (last header))
       (exports (map symbol->string exports0))
       (header-len (length header))
       (inames (filter identity (map (get-iname header-len) (map cons (range header-len) header))))
       (dependencies (map import-location-from-iname inames))
       (my-new-body rest)
       (previous (map loop dependencies))
       (return (append (apply append previous) (list (cons built-path my-new-body)))))))

  (define ret
    (let ((h (make-hash-table)))
      (let loop ((buf ret0))
        (if (null? buf) (list)
            (let* ((cur-pair (car buf))
                   (index (car cur-pair))
                   (value (cdr cur-pair)))
              (if (hash-ref h index #f)
                  (loop (cdr buf))
                  (begin
                    (hash-set! h index #t)
                    (cons value (loop (cdr buf))))))))))

  (apply string-append (cons* interpreter-run-keyword " " guile-name "\n" ret)))

(define (inline built-path)
  (define text (inline#pure built-path))
  (with-new-temporary-file
   text temp

   (define bresult
     (with-build-inputs
      (list)
      ((build-dest path) ?)
      (guile-abstract-build temp)))
   (build-result-path bresult)))

;;;;;;;;;;;;;;;;;;;;;;;
;; guile-interpreter ;;
;;;;;;;;;;;;;;;;;;;;;;;

(define (import-location-from-iname iname)
  (append-posix-path (guile-dir)
                     (string-append iname ".scm")))

(define (import-location-from-hash hash)
  (import-location-from-iname (hash->import-name hash)))

(define (import-statement-from-hash hash names)
  (let ((iname (hash->import-name hash)))
    (string-append
     "   :use-module ((" iname ")\n"
     "                 :select " (~a (map symbol->string names)) ")")))

(define (import-from-statements statements)
  (string-append
   (apply string-append (list-intersperse "\n" statements))
   "\n"))

(define (import-from-hashes hashes+names)
  (import-from-statements
   (map (lambda (o)
          (import-statement-from-hash (car o) (cdr o)))
        hashes+names)))

(define (import-from-packaged-dependencies pdeps)
  (let* ((hashes (hashes-of-packaged-dependencies pdeps))
         (names (map dependency-variables pdeps))
         (hashes+names (map cons hashes names)))
    ;; (pretty-print pdeps)
    ;; (message "hashes: ~a" hashes+names)
    (import-from-hashes hashes+names)))

(define (exports-from-vars vars)
  (define names (map preprocessor-line-args vars))
  (string-append "   :export " (~a names)))

(define (get-header hash pdeps vars)
  (define iname (hash->import-name hash))
  (define imports (import-from-packaged-dependencies pdeps))
  (define exports (exports-from-vars vars))
  (string-append "(define-module (" iname ")\n" imports "\n" exports ")"))

(define (hash->import-name hash)
  (string-append "x" (~a hash)))

(define (hash-saved? hash)
  (let* ((destination (import-location-from-hash hash)))
    (file-or-directory-exists? destination)))

(define guile-name "guile")

(define-directory guile-dir
  (append-posix-path (get-import-root-prefix) guile-name))

(define (guile-run exec? inputs srcfile args)
  (define args*
    (apply
     string-append
     (map
      (lambda (arg)
        (string-append " '" arg "' "))
      args)))

  (define bresult
    (read-list
     (with-input-from-string (string-append inputs "(build-dest path)?")
       (lambda _
         (with-output-to-string
           (lambda _
             (iprog-interp-build srcfile)))))))
  (define path (build-result-path bresult))
  (unless path
    (throw 'wtf: bresult))

  (apply cmd-echo (cons* "RUN" path args))
  (if exec?
      (begin
        (cache-save-to-file!)
        (apply execlp (cons* "guile" "guile" "-L" (guile-dir) "-s" path args)))
      (status:exit-val
       (apply system*/exit-code (cons* "guile" "guile" "-L" (guile-dir) "-s" path args)))))

(define (guile-package filepath)
  (sourcefile-hash (iprog-interp-package filepath)))

(define (guile-package-toplevel filepath)
  (display (guile-package filepath)))

(define (guile-build-toplevel filepath)
  (iprog-interp-build filepath))

(define (guile-run-toplevel query0 argv0)
  (define target (car argv0))
  (define argv (cdr argv0))
  (define query (if (string-null? query0) ""
                    (substring query0 (string-length query-cli-prefix))))
  (exit
   (intermezzo
    result (cache-save-to-file!)
    (guile-run #t query target argv))))

(define (guile-install-toplevel query0 argv0)
  (define srcfile (car argv0))
  (define linkpath (cadr argv0))
  (define query (if (string-null? query0) ""
                    (substring query0 (string-length query-cli-prefix))))

  (define bresult
    (read-list
     (with-input-from-string (string-append query "(hash hash)? (build-dest path)?")
       (lambda _
         (with-output-to-string
           (lambda _
             (iprog-interp-build srcfile)))))))
  (define path (build-result-path bresult))
  (define register-path (and path (remove-common-prefix path (get-czempak-root-prefix))))
  (define hash (build-result-hash bresult))
  (unless (and path hash)
    (throw 'wtf: bresult))

  (apply cmd-echo (list "LINK" path linkpath))
  (write-string-file linkpath
                     (stringf "#! /bin/sh\nexec guile -L '~a' -s '~a' \"$@\"\n"
                              (guile-dir) path))
  (system (stringf "chmod +x '~a'" linkpath))
  (append-string-file (get-install-table-file)
                      (string-append (get-fullpath linkpath)
                                     " " hash " " register-path "\n")))

(define (guile-list-dependencies-toplevel target)
  (for-each
   (lambda (d)
     (display d) (newline))
   (iprog-interp-get-dependencies target)))

(define (parse-arg index)
  (let ((args (get-command-line-arguments)))
    (unless (> (length args) index)
      (errorwith "Not enough CLI arguments"))
    (list-ref args index)))

(define (get-argv)
  (let* ((args (get-command-line-arguments))
         (d (drop-while (lambda (s) (not (equal? "--" s))) args))
         (dd (if (pair? d) (cdr d) d)))
    (when (null? dd)
      (errorwith "No program specified in ~s" args))
    dd))

(define (guile-interpreter-entry args)
  (parameterize ((get-command-line-arguments args))
    (czempak-top-level-guard
     (let* ((action (parse-arg 0))
            (target (parse-arg 1))
            (query target)
            (lst (list (cons "run" (lambda _ (guile-run-toplevel query (get-argv))))
                       (cons "build" (lambda _ (guile-build-toplevel target)))
                       (cons "install" (lambda _ (guile-install-toplevel query (get-argv))))
                       (cons "package" (lambda _ (guile-package-toplevel target)))
                       (cons "list-dependencies" (lambda _ (guile-list-dependencies-toplevel target)))))
            (get (assoc action lst))
            (fn (or (and get (cdr get))
                    (lambda _ (errorwith "Bad action ~s, expected one of: ~a" action
                                         (apply string-append (list-intersperse " " (map (compose ~s car) lst))))))))
       (fn)
       (cache-save-to-file!)))))

(hash-set!
 loaded-interpreters-hashmap
 "guile" guile-interpreter-entry)
