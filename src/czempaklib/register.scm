;;;; Copyright (C) 2021, 2022  Otto Vamenheis
;;;;
;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation; version 3 of the License.
;;;;
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (czempaklib register))

(use-modules (euphrates common)
             (tiny-logic libsrc)
             (tiny-logic opsrc)
             (tiny-logic repllibsrc)
             (czempaklib helpers)
             (ice-9 regex)
             (ice-9 rdelim)
             (ice-9 match))

(export register-entry
        register-entry-id
        register-entry-dependencies-str
        register-entry-dependencies
        register-entry-depend-on-me

        append-to-register-structure
        read-register-structure
        read-binregister-structure
        write-register-structure
        write-binregister-structure
        take-subgraph-of)

(define-rec register-entry
  id
  dependencies-str
  dependencies
  depend-on-me)

(define (append-to-register-structure
         register-struct packages-list)
  "`package-list : listof elem'
where `elem : (cons parent-hash dependency-hashes)'
where `parent-hash : string' and `dependency-hashes : listof string'
"
  (define h register-struct)

  (define (l-map elem)
    (let* ((parent-hash (car elem))
           (dependency-hashes (cdr elem))
           (entry (register-entry parent-hash
                                  dependency-hashes
                                  (list) (list))))
      (hash-set! h parent-hash entry)
      entry))
  (define l (map l-map packages-list))

  (define (connector entry dep)
    (define s (hash-ref h dep #f))

    (if s
        (begin
          (set-register-entry-depend-on-me!
           s
           (cons entry (register-entry-depend-on-me s)))

          (set-register-entry-dependencies!
           entry
           (cons s (register-entry-dependencies entry))))
        (parameterize ((current-output-port (current-error-port)))
          (message "oh no! registry is broken, missing dependency:\n~a <- ~a"
                    dep (register-entry-id entry))))

    #f)
  (define (l-foreach entry)
    (for-each
     (lambda (dep) (connector entry dep))
     (register-entry-dependencies-str entry)))
  (for-each l-foreach l)

  l)

(define (read-graph-structure file)
  (define lns
    (lines (read-string-file file)))

  (define filtered
    (filter (negate string-null?) lns))

  (define packages-list
    (map words filtered))

  (define h (make-hash-table))
  (append-to-register-structure h packages-list)
  h)

(define (read-register-structure)
  (read-graph-structure (get-register-graph-file)))
(define (read-binregister-structure)
  (read-graph-structure (get-binregister-graph-file)))

(define (write-graph-structure G file)
  (define p (open-output-file file))

  (hash-table-foreach
   G
   (lambda (key value)
     (define ids (map register-entry-id (register-entry-dependencies value)))
     (display key p)
     (display " " p)
     (display (unwords ids) p)
     (newline p)))

  (close-port p))

(define (write-register-structure G)
  (write-graph-structure G (get-register-graph-file)))
(define (write-binregister-structure G)
  (write-graph-structure G (get-binregister-graph-file)))

;; G: hashmap (such as register-structure)
;; nodes: list of strings (ids of vertexes)
(define (take-subgraph-of G nodes)
  (define (get-vertex id)
    (hash-ref G id #f))
  (define (get-vertex-by-entry entry)
    (define id (register-entry-id entry))
    (get-vertex id))

  (define M (make-hash-table))

  (define (loop v)
    (let lp ((v v))
      (unless (hash-ref M (register-entry-id v) #f)
        (hash-set! M (register-entry-id v) v)
        (for-each
         (compose lp get-vertex-by-entry)
         (register-entry-dependencies v)))))

  (define vs
    (map (lambda (node-id)
           (or (get-vertex node-id)
               (throw 'node-to-rewrite-not-found node-id)))
         nodes))

  (for-each loop vs)

  M)
