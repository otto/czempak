;;;; Copyright (C) 2020, 2021  Otto Vamenheis
;;;;
;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation; version 3 of the License.
;;;;
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (czempaklib helpers)
  :export
  (
   choose-args
   parameterize-fn
   cached-record-accessor
   create-empty-file
   replace-words
   replace-words-in-lines
   temporary-file?
   local-file?
   global-file?
   get-file-type
   get-fullpath
   write-to-temporary
   with-new-temporary-file
   czempak-read-file
   get-register-graph-file
   get-binregister-graph-file
   get-install-table-file
   register-package
   register-bin
   make-directory#transitive
   define-directory
   czempak-root-override
   czempak-root-override-variable
   get-czempak-root-prefix
   get-import-root-prefix
   get-package-store-directory
   get-package-root-directory
   get-pots-root
   get-czempak-temp-directory
   package-location-from-hash
   get-current-link
   message
   failwith
   errorwith
   czempak-top-level-guard
   cmd
   cmd-re
   cmd-echo
   cmd-echo-f
   download
   move-source
   copy-source
   query-cli-prefix
   query-inputs-name
   query-environment-name
   build-input-stream
   current-environment
   with-build-inputs
   query-result-find-multiple
   query-result-find-single
   build-result-hash
   build-result-final-hash
   build-result-path
   sourcefile
   sourcefile-path
   sourcefile-%hash
   set-sourcefile-%hash!
   sourcefile-%content
   set-sourcefile-%content!
   make-sourcefile
   make-sourcefile-from-hash
   %sourcefile-cache
   interpreter-run-keyword-prefix
   interpreter-run-keyword-str
   interpreter-run-keyword
   interpreter-run-keyword-length
   get-port-interpreters
   get-build-interpreters
   loaded-interpreters-hashmap
   get-interpreter-executable
   interpreter-do
   interpreter-build
   interpreter-package
   package
   build
   list-dependencies
  ))

(use-modules (euphrates common))
(use-modules (ice-9 regex))
(use-modules (ice-9 rdelim)) ;; read-line

;;;;;;;;;;;;;
;; HELPERS ;;
;;;;;;;;;;;;;

;; TODO: rewrite as a macro
(define (choose-args . is)
  (define (mem x)
    (member x is))
  (lambda args
    (apply values
           (map car (filter (compose mem cdr)
                            (map cons args (range (length args))))))))

(define-syntax-rule (parameterize-fn alist fn)
  (lambda args
    (parameterize alist
      (apply fn args))))

(define (cached-record-accessor get reload set)
  (lambda (f)
    (or (get f)
        (let ((new (reload f)))
          (set f new)
          new))))

(define (create-empty-file name)
  (write-string-file name ""))

(define (replace-words alist in-text)

  (define (tostring x)
    (cond
     ((string? x) x)
     ((symbol? x) (symbol->string x))
     ((number? x) (number->string x))
     (else (throw 'bad-alist-in-replace-words alist))))

  (define (replace-word word by in-text)
    (regexp-substitute/global
     #f
     (tostring word) in-text
     'pre (tostring by) 'post))

  (let lp ((t in-text)
           (rest alist))
    (if (null? rest)
        t
        (let* ((top (car rest))
               (new (replace-word (car top) (cdr top) t)))
          (lp new (cdr rest))))))

;; accepts in-lines some of which may not be `string?`
;; returns new lines with order preserved
(define (replace-words-in-lines alist in-lines)
  (if (null? alist) in-lines
      (let ((squashed
             (let lp ((rest in-lines) (mem #f) (buf (list)))
               (if (null? rest)
                   (if mem
                       (reverse (cons mem buf))
                       (reverse buf))
                   (let ((cur (car rest)))
                     (if (string? cur)
                         (lp (cdr rest)
                             (if mem (string-append mem "\n" cur) cur)
                             buf)
                         (lp (cdr rest)
                             #f
                             (cons cur (if mem (cons mem buf) buf)))))))))
            (let lp ((buf squashed))
              (if (null? buf) (list)
                  (let ((cur (car buf)))
                    (if (string? cur)
                        (let ((repl (replace-words alist cur)))
                          (append (lines repl) (lp (cdr buf))))
                        (cons cur (lp (cdr buf))))))))))

(define (temporary-file? path)
  (string-prefix? czempak-temp-directory#value#prefixed path))
(define (global-file? path)
  (string-prefix? (get-package-root-directory) path))
(define (local-file? path)
  (equal? 'local (get-file-type path)))

;; TODO: improve this check
(define (get-file-type path)
  (cond
   ((temporary-file? path) 'temp)
   ((global-file? path) 'global)
   (else 'local)))

;;;;;;;;;;;;
;; SHARED ;;
;;;;;;;;;;;;

(define (get-fullpath path)
  (if (string-prefix? "/" path) path
      (simplify-posix-path (append-posix-path (getcwd) path))))

(define temporary-files-table
  (make-hash-table))

(define (get-temporary-path)
  (append-posix-path (get-czempak-temp-directory)
                     (~a (big-random-int 999999))))

(define (write-to-temporary original-path . parts)
  (let ((path (get-temporary-path)))
    (cmd-echo "TEMP" path original-path)

    (hash-set! temporary-files-table
               path
               (apply string-append parts))

    path))

(define-syntax-rule (with-new-temporary-file text path . bodies)
  (let* ((path (write-to-temporary "" text))
         (_ (write-string-file path (czempak-read-file path)))
         (result (let () . bodies)))
    ;; (delete-file path)
    result))

(define (czempak-read-file path)
  (if (temporary-file? path)
      (hash-ref temporary-files-table path #f)
      (read-string-file path)))

(define (czempak-call-with-input-file path body)
  (if (temporary-file? path)
      (call-with-input-string (hash-ref temporary-files-table path #f) body)
      (call-with-input-file path body)))

(define-syntax-rule (define-file name path)
  (define name
    (memconst
     (let ((ret path))
       (unless (file-or-directory-exists? ret)
         (create-empty-file ret))
       ret))))

(define-file get-register-graph-file
  (append-posix-path (get-package-store-directory) "register.dat"))

(define-file get-binregister-graph-file
  (append-posix-path (get-package-store-directory) "binregister.dat"))

(define-file get-install-table-file
  (append-posix-path (get-package-store-directory) "install.dat"))

(define (register-package hash dependency-hashes)

  (define deps
    (or dependency-hashes (list)))

  (define row-data
    (cons hash deps))
  (define row
    (string-append (unwords row-data) "\n"))

  (apply cmd-echo (cons* "REG" hash dependency-hashes))
  (append-string-file (get-register-graph-file) row))

(define (register-bin hash dependency-paths)
  (define (shorten-path path)
    (remove-common-prefix path (get-czempak-root-prefix)))

  (define deps
    (or dependency-paths (list)))

  (define row-data0
    (cons hash deps))
  (define row-data
    (map shorten-path row-data0))

  (define row
    (string-append (unwords row-data) "\n"))

  (apply cmd-echo (cons "RBIN" row-data0))
  (append-string-file (get-binregister-graph-file) row))

(define-syntax list-fold*
  (syntax-rules ()
    ((_ ((acc-x . acc-xs) acc-value)
        (i-name i-value)
        . bodies)
     (let loop ((acc-list (call-with-values (lambda _ acc-value) (lambda x x)))
                (i-all i-value))
       (if (null? i-all) (apply values acc-list)
           (let ((i-name (car i-all)))
             (call-with-values
                 (lambda _ (apply values acc-list))
               (lambda (acc-x . acc-xs)
                 (call-with-values (lambda _ . bodies)
                   (lambda new-acc
                     (loop new-acc (cdr i-all))))))))))
    ((_ (acc-name acc-value)
        (i-name i-value)
        . bodies)
     (let loop ((acc-name acc-value) (i-all i-value))
       (if (null? i-all) acc-name
           (let ((i-name (car i-all)))
             (let ((new-acc (let () . bodies)))
               (loop new-acc (cdr i-all)))))))))

(define (make-directories path)
  (define mk-single-dir mkdir)
  (define parts (string-split path #\/))
  (list-fold*
   (acc #f)
   (i parts)
   (let* ((path0 (if acc (append-posix-path acc i) i))
          (path (if (string-null? path0) "/" path0)))
     (unless (file-exists? path)
       (mk-single-dir path))
     path)))

(define (make-directory#transitive path)
  (cmd-echo "MKDIR" path)
  (make-directories path))

(define-syntax-rule (get-or-make-directory value)
  (memconst
   (intermezzo
    result (unless (file-or-directory-exists? result) (make-directory#transitive result))
    value)))

(define-syntax-rule (define-directory name value)
  (define name (get-or-make-directory value)))

(define czempak-root-override-variable
  "CZEMPAK_ROOT")
(define czempak-root-override
  (getenv czempak-root-override-variable))

(define czempak-root-prefix#value
  (or czempak-root-override
      (append-posix-path (getenv "HOME")
                         ".local" "share" "czempak")))
(define-directory get-czempak-root-prefix
  czempak-root-prefix#value)

(define package-store-directory#value
  (append-posix-path czempak-root-prefix#value "store"))
(define-directory get-package-store-directory
  package-store-directory#value)

(define import-root-prefix#value
  (append-posix-path package-store-directory#value "bin"))
(define-directory get-import-root-prefix
  import-root-prefix#value)

(define package-root-directory#value
  (append-posix-path package-store-directory#value "src"))
(define-directory get-package-root-directory
  package-root-directory#value)

(define pots-root#value
  (append-posix-path czempak-root-prefix#value "pots"))
(define-directory get-pots-root
  pots-root#value)

(define czempak-temp-directory#value
  (append-posix-path czempak-root-prefix#value "temp"))
(define-directory get-czempak-temp-directory
  czempak-temp-directory#value)
(define czempak-temp-directory#value#prefixed
  (string-append czempak-temp-directory#value "/"))

(define (package-location-from-hash hash)
  (append-posix-path (get-package-root-directory) hash))

(define cache-signaturefile-path ".czempak-link")
(define get-current-link
  (memconst
   (catch-any
    (lambda _ (read-string-file cache-signaturefile-path))
    (lambda _
      (let ((link (~a (big-random-int 9999999999999999))))
        (write-string-file cache-signaturefile-path link)
        link)))))

;;;;;;;;;;;;;;
;; cmd-echo ;;
;;;;;;;;;;;;;;

(define czempak-verbosity
  (string->number
   (or (getenv "CZEMPAK_VERBOSITY") "10")))

(define (message . args)
  (apply cmd-echo-f (cons "INFO" args)))

(define (failwith . args)
  (apply throw (cons 'fail args)))
(define (errorwith . args)
  (apply throw (cons 'error args)))

(define-syntax-rule (czempak-top-level-guard . bodies)
  (let ((error #f))
    (catch 'fail
           (lambda _
             (catch 'error
                    (lambda _ . bodies)
                    (lambda (key . args)
                      (apply cmd-echo-f (cons "ERROR" args))
                      (set! error #t))))
           (lambda (key . args)
             (apply cmd-echo-f (cons "FAIL" args))
             (set! error #t)))
    (if error (exit 1) (exit 0))))

(define cmd-input-stream
  (make-parameter #f))

(define (cmd program . args)
  (cond
   ((string? program)
    (let* ((p (with-no-shell-log (apply sh-async (cons program args))))
           (pipe (comprocess-pipe p)))
      (when (cmd-input-stream)
        (display (cmd-input-stream) pipe)
        (close-port pipe))
      (sleep-until (comprocess-exited? p))
      (shell-check-status p)))
   ((procedure? program)
    (with-output-to-string
      (lambda ()
        (with-input-from-string
            (or (cmd-input-stream) "")
          (lambda ()
            (program args))))))
   (else
    (throw 'program-is-expected-to-be-either-string-or-procedure program))))

(define (cmd-re program . args)
  (string-trim-chars
   (with-output-to-string
     (lambda _ (apply cmd (cons program args))))
   "\n \t" 'both))

(define cmd-echo-max-len 100)
(define cmd-echo-pad-len 6)
(define cmd-echo-pad
  (case-lambda
   ((str) (cmd-echo-pad str 0))
   ((str diff) (string-pad-right str (+ cmd-echo-pad-len diff) #\space))))

(define (cmd-echo#raw name . args)
  (define (print x) (display x (current-error-port)))
  (print (cmd-echo-pad name))
  (let loop ((args args) (count cmd-echo-pad-len))
    (unless (null? args)
      (let* ((cur (car args))
             (len (string-length cur))
             (len+1 (+ 1 len))
             (break? (and (> (+ count len) cmd-echo-max-len)
                          (> (+ count len) (+ cmd-echo-pad-len len)))))
        (when break?
          (print (cmd-echo-pad "\n " 1)))
        (print cur) (print " ")
        (loop (cdr args) (if break? (+ cmd-echo-pad-len len+1) (+ count len+1))))))
  (print "\n"))

(define cmd-echo-temporary-file-table
  (make-hash-table))
(define cmd-echo-temporary-file-counter
  0)
(define (cmd-echo-temporary-get-or-add file)
  (let ((num
         (let ((o (hash-ref cmd-echo-temporary-file-table file #f)))
           (or o (begin (set! cmd-echo-temporary-file-counter (+ 1 cmd-echo-temporary-file-counter))
                        (hash-set! cmd-echo-temporary-file-table file cmd-echo-temporary-file-counter)
                        cmd-echo-temporary-file-counter)))))
    (string-append "$" (number->string num))))

(define (cmd-echo-force name args-full)
  (define replace-alist
    (filter car (list (cons import-root-prefix#value "$BIN")
                      (cons package-root-directory#value "$SRC")
                      (cons pots-root#value "$POTS")
                      (cons package-store-directory#value "$STORE")
                      (cons czempak-root-prefix#value "$ROOT"))))
  (define (simplify p)
    (if (temporary-file? p)
        (cmd-echo-temporary-get-or-add p)
        (replace-words replace-alist (~a p))))
  (define args
    (map simplify args-full))
  (apply cmd-echo#raw (cons name args)))

(define (cmd-echo name . args-full)
  (when (> czempak-verbosity 50)
    (cmd-echo-force name args-full)))

(define (cmd-echo-f name . args-full)
  (apply cmd-echo (cons name (words (apply stringf args-full)))))

;; downloads package and stores in package-path
(define (download hash)
  (cmd "czempak-download ~s" hash))

(define (copy-source source-path destination-path)
  (cmd-echo "COPY" source-path destination-path)
  (if (temporary-file? source-path)
      (write-string-file destination-path (czempak-read-file source-path))
      (copy-file source-path destination-path)))
(define (move-source source-path destination-path)
  (cmd-echo "MOVE" source-path destination-path)
  (if (temporary-file? source-path)
      (write-string-file destination-path (czempak-read-file source-path))
      (rename-file source-path destination-path)))

;;;;;;;;;;;
;; QUERY ;;
;;;;;;;;;;;

(define query-cli-prefix "query=")

(define query-inputs-name 'inputs)
(define query-environment-name 'environment)

(define build-input-stream (make-parameter #f))

(define current-environment
  (make-parameter 'current-environment-not-initialized))

(define (input-args->string name args)
  (if (null? args) ""
      (apply string-append
             (map ~s (apply append (map (lambda (i) (list (list name i) '!)) args))))))

(define-syntax-rule (with-build-inputs inputs query . bodies)
  (let* ((in0 (input-args->string query-environment-name (current-environment)))
         (in1 (input-args->string query-inputs-name inputs))
         (in2 (apply string-append (map ~s (quote query))))
         (in (string-append in0 in1 in2)))
    (read-list
     (parameterize ((build-input-stream in))
       . bodies))))

(define (query-result-find-multiple key)
  (lambda (bresult)
    (apply
     append
     (map (lambda (e)
            (filter
             identity
             (map (lambda (p)
                    (and (equal? (car p) key) (cdr p))) e)))
          (filter pair? bresult)))))

(define (query-result-find-single key)
  (let ((mult (query-result-find-multiple key)))
    (lambda (bresult)
      (let ((x (mult bresult)))
        (cond
         ((null? x) #f)
         ((null? (cdr x)) (car x))
         (else (throw 'multiple-values-in-find-single key bresult)))))))

(define build-result-final-hash (query-result-find-single 'final-hash))
(define build-result-hash (query-result-find-single 'hash))
(define build-result-path (query-result-find-single 'path))

;;;;;;;;;;;;;;;;
;; SOURCEFILE ;;
;;;;;;;;;;;;;;;;

(define-rec sourcefile
  path ;; : string?
  %hash ;; : string? | #f
  %content ;; : string? | #f
  )

;; TODO: use absolute paths
(define (make-sourcefile path)
  (or (hash-ref %sourcefile-cache path #f)
      (intermezzo
       result (hash-set! %sourcefile-cache path result)
       (sourcefile path #f #f))))

(define (make-sourcefile-from-hash hash)
  (make-sourcefile (package-location-from-hash hash)))

;; TODO: memoize local files as well
;; keys are paths, values are sourcefiles
(define %sourcefile-cache
  (make-hash-table))

;;;;;;;;;;;;;;;;;
;; interpreter ;;
;;;;;;;;;;;;;;;;;

(define interpreter-run-keyword-prefix "%")
(define interpreter-run-keyword-str "run")
(define interpreter-run-keyword
  (string-append interpreter-run-keyword-prefix interpreter-run-keyword-str))
(define interpreter-run-keyword-length
  (string-length interpreter-run-keyword))

(define (get-port-interpreters file)
  (let ((result
         (let loop ()
           (let ((line (read-line file)))
             (if (eof-object? line) (list)
                 (if (string-prefix? interpreter-run-keyword line)
                     (let* ((ws (substring line interpreter-run-keyword-length))
                            (i (or (string-index ws #\space)
                                   (string-length ws)))
                            (rr (substring ws i))
                            (r (map (curry-if symbol? symbol->string) (read-list rr))))
                       r)
                     (loop)))))))
    (close-port file)
    result))

(define (get-file-interpreters path)
  (czempak-call-with-input-file path
    (lambda (p) (get-port-interpreters p))))

(define (get-content-interpreters content)
  (call-with-input-string content
    (lambda (p) (get-port-interpreters p))))

(define fallback-interpreter (make-parameter #f))
(define (get-fallback-interpreter srcfile)
  (or (fallback-interpreter)
      (parameterize ((current-output-port (current-error-port)))
        (failwith "Do not know which interpreter to use for ~s" srcfile))))

;; returns non-empty list of interpreters or throws an error
(define (get-build-interpreters srcfile)
  (let* ((read-interps (get-file-interpreters srcfile))
         (interps
          (or (and (pair? read-interps) read-interps)
              (list (cons (get-fallback-interpreter srcfile) "fallback")))))
    interps))

;;;;;;;;;;;
;; BUILD ;;
;;;;;;;;;;;

(define loaded-interpreters-hashmap
  (make-hash-table))

(define (get-interpreter-executable interp-id)
  (define loaded (hash-ref loaded-interpreters-hashmap
                           interp-id #f))
  (or loaded
      (throw 'TODO:support-external-interpreters interp-id)))

(define (interpreter-do fn)
  (lambda (srcfile)
    (let* ((interps (get-build-interpreters srcfile))
           (chosen (car interps)))
      ((fn chosen) interps srcfile))))

(define (interpreter-package interp-id)
  (let ((executable (get-interpreter-executable interp-id)))
    (lambda (interps srcfile)
      (cmd-echo-f "CALL" "~a package ~s" executable srcfile)
      (cmd-re executable "package" srcfile))))
      ;; (cmd "czempak-interpret ~s ~s" interp-id srcfile)))

(define (interpreter-build interp-id)
  (let ((executable (get-interpreter-executable interp-id)))
    (lambda (interps srcfile)
      (cmd-echo-f "CALL" "echo '~a' | ~a build ~s" (build-input-stream) executable srcfile)
      (parameterize ((cmd-input-stream (build-input-stream)))
        (cmd-re executable "build" srcfile)))))

(define (interpreter-list-dependencies interp-id)
  (let ((executable (get-interpreter-executable interp-id)))
    (lambda (interps srcfile)
      (cmd-echo-f "CALL" "~a list-dependencies ~s" executable srcfile)
      (cmd executable "list-dependencies" srcfile))))
      ;; (cmd "czempak-interpret ~s ~s" interp-id srcfile)))

(define package
  (interpreter-do interpreter-package))

(define build
  (interpreter-do interpreter-build))

(define list-dependencies
  (interpreter-do interpreter-list-dependencies))

