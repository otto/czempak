%run guile

%var say-hi
%var say-hello
%var kek-var

(define kek-var 22)

%for (language @int)

(define-syntax-rule [say-hi]
  (begin
    (display "kek language: @int\n")
    (display "hi, hi @int\n")))

%end

(define [say-hello]
  (display "hello\n"))

(display "in kek.scm 2dd19992\n")

