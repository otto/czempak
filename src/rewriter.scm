;;;; Copyright (C) 2020, 2021, 2022  Otto Vamenheis
;;;;
;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation; version 3 of the License.
;;;;
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(use-modules (euphrates common)
             (tiny-logic libsrc)
             (tiny-logic opsrc)
             (tiny-logic repllibsrc)
             (czempaklib helpers)
             (czempaklib register)
             (ice-9 regex)
             (ice-9 rdelim)
             (ice-9 match))

;;;;;;;;;;;;;
;; rewrite ;;
;;;;;;;;;;;;;

(define (register-get-entry register-structure hash)
  (hash-ref register-structure hash #f))

(define input-struct-elem-target cdr)
(define input-struct-elem-input car)

(define (make-dependency-replacer input-struct)
  (define (app elem)
    (define target (input-struct-elem-target elem))
    (define input (input-struct-elem-input elem))

    (string-append "s/" input "/" target "/g"))

  (define str
    (apply string-append
           (list-intersperse " ; " (map app input-struct))))

  (define (return input-file output-file)
    (cmd-echo "SED" str input-file)
    (cmd "sed '~a' '~a' >~a"
         str input-file output-file))

  return)

(define (parse-replace-dependencies input-args)
  (map (lambda (arg)
         (let ((lst (string-split#simple arg #\:)))
           (cons (car lst) (cadr lst))))
       input-args))

(define (replace-dependencies input-struct target-file output-file)
  (define replacer (make-dependency-replacer input-struct))
  (replacer target-file output-file))

(define-rec rewrite-vertex
  entry ;; register entry
  dependencies ;; links to vertexes that this depends on
  ref-count ;; how many dependencies are not yet rewritten?
  next-version-hash ;; next version of this vertex (if this one was rewritten)
  initial? ;; initial vertexes are not built
  depend-on-me ;; vertexes that depend on me
  )

(define (rewrite-single#inplace input-struct target-file)
  (define temp (make-temporary-filename))
  (replace-dependencies input-struct target-file temp)
  (sourcefile-path (move-source temp target-file)))

(define (rewrite-single input-struct target-file)
  (define temp (make-temporary-filename))
  (define _
    (replace-dependencies input-struct target-file temp))
  (define r-src (package temp))
  (with-ignore-errors! (delete-file temp))
  r-src)

(define (rewrite#parsed input-struct target-files)
  (define (get-hash file)
    (cons file (list-dependencies file)))

  (define mappings
    (map get-hash target-files))

  (define hashes
    (list-deduplicate
     (apply append (map cdr mappings))))

  (define subgraph
    (take-subgraph-of (read-register-structure) hashes))

  (define result
    (rewrite-entries-smart subgraph input-struct))

  (define (rewrite-target-file mapping)
    (define file (car mapping))
    (define deps (cdr mapping))

    (define (pair-up d)
      (let* ((v (hash-ref result d #f))
             (e (and v (rewrite-vertex-entry v))) ;; v could not exist if it was not rewritten
             (cur-hash (and e (register-entry-id e))) ;; e could not exist if nobody depended on it
             (next-hash (and cur-hash (rewrite-vertex-next-version-hash v)))) ;; TODO: investigate all cases where vertexes dont exist
        (and next-hash (cons cur-hash next-hash))))

    (define input-struct
      (filter identity (map pair-up deps)))

    (unless (null? input-struct)
      (rewrite-single#inplace input-struct file))

    (cons file (not (null? input-struct))))

  (for-each rewrite-target-file mappings))

(define (rewrite input-args target-files)
  (define input-struct
    (parse-replace-dependencies input-args))
  (rewrite#parsed input-struct target-files))

(define (rewrite-entries-smart
         register-struct
         input-struct)
  (define _
    (cmd-echo "REWR" input-struct))

  (define all-inputs
    (map input-struct-elem-input input-struct))

  (define h
    (make-hash-table))

  (define (create-vertex-from-entry id entry next-version-hash)
    ;; create new if not found
    (define (get-vertex entry)
      (define id (register-entry-id entry))
      (or (hash-ref h id #f)
          (create-vertex-from-entry id entry #f)))

    (define vertexes-that-depend-on-me
      (if entry
          (map get-vertex
               (register-entry-depend-on-me entry))
          (list)))
    (define ret
      (rewrite-vertex
       entry ;; entry
       (list) ;; dependencies
       0 ;; ref-count
       next-version-hash ;; next-version-hash
       (not (not next-version-hash)) ;; initial?
       vertexes-that-depend-on-me ;; depend-on-me
       ))
    (hash-set! h id ret)
    ret)

  (define (append-dependency-to-vertex dep v)
    (set-rewrite-vertex-dependencies!
     v (cons dep (rewrite-vertex-dependencies v)))

    ;; assumption: `dep' is added once!
    ;; TODO: check duplicates?
    (set-rewrite-vertex-ref-count!
     v (1+ (rewrite-vertex-ref-count v))))

  (define (visit-vertex v)
    (define entry
      (rewrite-vertex-entry v))

    ;; entry could not exist for initial vertexes - it's fine
    (if (not entry) (list)
        (let* ((vertexes-that-depend-on-me
                (rewrite-vertex-depend-on-me v))
               (append-and-filter
                (lambda (depends-on-me)
                  (define already-visited?
                    (or (> (rewrite-vertex-ref-count depends-on-me) 0)
                        (rewrite-vertex-initial? depends-on-me)))

                  (append-dependency-to-vertex v depends-on-me)

                  ;; don't need to revisit vertex that was already visited
                  ;; or initial vectors
                  (if already-visited? #f
                      depends-on-me)))
               (ret0
                (map append-and-filter vertexes-that-depend-on-me))
               (ret-filtered
                (filter identity ret0)))
          ret-filtered)))

  (define-values
      (enqueue-build-vertex
       dequeue-build-vertex)
    (let ((build-queue (list)))
      (define (enqueue v)
        (set! build-queue
              (cons v build-queue)))
      (define (dequeue)
        (if (null? build-queue) #f
            (let ((ret (car build-queue)))
              (set! build-queue (cdr build-queue))
              ret)))
      (values enqueue dequeue)))

  (define (finish-build vertex)
    (define entry
      (rewrite-vertex-entry vertex))

    (when entry
      (let ((depend-on-me
             (rewrite-vertex-depend-on-me vertex))
            (decrease-refcount
             (lambda (v)
               (set-rewrite-vertex-ref-count!
                v (1- (rewrite-vertex-ref-count v))))))

        (for-each decrease-refcount depend-on-me)
        (for-each enqueue-build-vertex depend-on-me))))

  (define (rewrite-vertex-id v)
    (register-entry-id (rewrite-vertex-entry v)))

  (define (go-build vertex)
    (define already-built?
      (rewrite-vertex-next-version-hash vertex))
    (unless already-built?
      (let* ((dependencies
              (rewrite-vertex-dependencies vertex))
             (get-input-struct-elem
              (lambda (dependent-vertex)
                (cons
                 (rewrite-vertex-id dependent-vertex)
                 (rewrite-vertex-next-version-hash dependent-vertex))))
             (input-struct
              (map get-input-struct-elem dependencies))
             (hash
              (rewrite-vertex-id vertex))
             (input-file
              (package-location-from-hash hash))
             (next-version-hash
              (rewrite-single input-struct input-file)))

        (set-rewrite-vertex-next-version-hash!
         vertex next-version-hash))

      (finish-build vertex)))

  (define (start-build-loop)
    (let lp ()
      (let ((v (dequeue-build-vertex)))
        (when v
          (when (= 0 (rewrite-vertex-ref-count v))
            (go-build v))
          (lp)))))

  ;; make input vertexes
  (define all-input-vertexes
    (map
     (lambda (elem)
       (define input (input-struct-elem-input elem))
       (define target (input-struct-elem-target elem))
       (define entry
         (register-get-entry register-struct input))

       (unless entry
         (message "package ~a not found.. ignoring" input))

       (create-vertex-from-entry input entry target))
     input-struct))

  (define all-input-entries
    (filter
     identity
     (map rewrite-vertex-entry all-input-vertexes)))

  ;; make the rest of the damn graph
  (let lp ((buf all-input-vertexes))
    (unless (null? buf)
      (let* ((new-bunches (map visit-vertex buf))
             (new (apply append new-bunches)))
        (lp new))))

  ;; initiate build by starting from initial vertexes
  (for-each finish-build all-input-vertexes)

  ;; run the engine
  (start-build-loop)

  h)


;;;;;;;;;;
;; MAIN ;;
;;;;;;;;;;

(define (rewrite#action)
  (define input-args (parse-cli-get-list "list"))
  (define target-files (parse-cli-get-list "target"))
  (rewrite input-args target-files))

